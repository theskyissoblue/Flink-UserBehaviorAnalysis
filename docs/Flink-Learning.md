# Flink 流批一体系统学习

[TOC]

# 第 1 章 Flink 简介

## 1.1 初识 Flink

​        Flink 起源于 Stratosphere 项目，Stratosphere 是在 2010~2014 年由 3 所地处柏林的大学和欧洲的一些其他的大学共同进行的研究项目，

 		-- 2014 年 4 月 Stratosphere 的代码被复制并捐赠给了 Apache 软件基金会，参加这个孵化项目的初始成员是Stratosphere 系统的核心开发人员。

​		-- 2014 年 12 月，Flink 一跃成为 Apache 软件基金会的顶级项目。

​		在德语中，Flink 一词表示快速和灵巧，项目采用一只松鼠的彩色图案作为 logo，这不仅是因为松鼠具有快速和灵巧的特点，还因为柏林的松鼠有一种迷人的红棕色，而 Flink 的松鼠 logo 拥有可爱的尾巴，尾巴的颜色与 Apache 软件基金会的 logo 颜色相呼应，也就是说，这是一只 Apache 风格的松鼠。

![image-20200730165318431](images/image-20200730165318431.png)

### 1.1.1 Flink 是什么

![image-20200730170253236](images/image-20200730170253236.png)

​		Flink 项目的理念是：==“Apache Flink 是为分布式、高性能、随时可用以及准确的流处理应用程序打造的开源流处理框架”==。

​		Apache Flink 是一个==框架==和==分布式==处理引擎，用于对==无界和有界数据流==进行有==状态==计算。Flink 被设计在所有常见的集群环境中运行，以内存执行速度和任意规模来执行计算。

![img](C:\Users\Lenovo\Desktop\高级数据架构师\项目\电商项目\2-分布式高级篇\flink-home-graphic.png)



### 1.1.2 Flink 的全球热度

![image-20200730170348404](images/image-20200730170348404.png)

### 1.2.3 Flink 目前在国内企业的应用

![image-20200730170433671](images/image-20200730170433671.png)

### 1.2.4 为什么选择 

- 流数据更真实地反映了我们的生活方式
- 传统的数据架构是基于有限数据集的
- 我们的目标
  - 低延迟
  - 高吞吐
  - 结果的准确性和良好的容错性

1.2.5 那些行业需要处理流数据

- 电商和市场营销
  - 数据报表、广告投放、业务流程需要

- 物联网
  - 传感器实时数据的采集和显示、实时报警、交通运输业
- 电信业
  - 基站流量调配
- 银行和金融业
  - 实时结算和通知推送，实时检测异常行为

1.2.5 传统数据处理架构

- 事务处理

![image-20200730171107070](images/image-20200730171107070.png)

- 分析处理

  - 将数据从业务数据库复制到数仓，再进行分析和查询

  ![image-20200730171238533](images/image-20200730171238533.png)

- 有状态的流式处理

  ![image-20200730171333221](images/image-20200730171333221.png)

- 流处理的演变

  ![image-20200730171629331](images/image-20200730171629331.png)

  - lambda 架构
    - 用两套系统，同时保证低延迟和结果准确
  - 

  ![image-20200730171527296](images/image-20200730171527296.png)



## 1.2 Flink 的主要特点

- ### 事件驱动(Event-driven)

​		事件驱动型应用是一类==具有状态==的应用，它从一个或多个事件流提取数据，并根据到来的事件触发计算、状态更新或其他外部动作。比较典型的就是以 ==kafka== 为代表的消息队列几乎都是事件驱动型应用。与之不同的就是 ==SparkStreaming 微批次==，如图

![image-20200730165710794](images/image-20200730165710794.png)

>  事件驱动型

![image-20200730171743138](images/image-20200730171743138.png)

- ### 基于流的世界观

  -  flink 的世界观中，一切都是由==流==组成的，离线数据是有界限的流，实时数据是一个没有界限的流，这就是所谓的有界流和无界流。

  ![image-20200730172021758](images/image-20200730172021758.png)

​		无界数据流：无界数据流有一个开始但是没有结束，它们不会在生成时终止并提供数据，必须连续处理无界流，也就是说必须在获取后立即处理 event。对于无界数据流我们无法等待所有数据都到达，因为输入是无界的，并且在任何时间点都不会完成。处理无界数据通常要求以特定顺序（例如事件发生的顺序）获取 event，以
便能够推断结果完整性。

​		有界数据流：有界数据流有明确定义的开始和结束，可以在执行任何计算之前通过获取所有数据来处理有界流，处理有界流不需要有序获取，因为可以始终对有界数据集进行排序，有界流的处理也称为批处理。

- ### 分层 API

  - 越顶层越抽象，表达含义越简明，使用越方便
  - 越底层越具体，表达能力越丰富，使用越灵活

  ![image-20200730172235273](images/image-20200730172235273.png)

- ### Flink 的其他特点

  - 支持事件时间（event-time）和处理时间（processing-time）语义；
  - 精确一次（exactly-once）的状态一致性保证；
  - 低延迟，每秒处理数百万个实践，毫秒级延迟；
  - 与众多常用存储系统的连接；
  - 高可用，动态扩展，实现 7*24 小时的全天候运行。

- Flink vs Spark Streaming

  - 流（stream）和微批（micro-batching）

    - ==批处理==的特点是有界、持久、大量，非常适合需要访问全套记录才能完成的计算工作，一般用于离线统计。
    - ==流处理==的特点是无界、实时, 无需针对整个数据集执行操作，而是对通过系统传输的每个数据项执行操作，一般用于实时统计。

    ![image-20200730172810378](images/image-20200730172810378.png)

  - 数据模型

    - spark 采用 RDD 模型，spark streaming 的 DStream 实际上也就是一组组小批数据 RDD 的集合。在 spark 的世界观中，一切都是由==批次==组成的，离线数据是一个大批次，而实时数据是由一个一个无限的小批次组成的。
    - flink 基本数据模型是数据流，以及事件（Event）序列。

  - 运行时架构

    - spark 是批计算，将 DAG 划分为不同的 stage，一个完成后才可以计算下一个。
    - flink 是标准的流执行模式，一个事件在一个节点处理完后可以直接发往下一个节点进行处理。

# 第 2 章 快速上手

## 2.1 搭建 maven 工程 FlinkTutorial

### 2.1.1 pom 文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.atguigu</groupId>
    <artifactId>FlinkTutorial</artifactId>
    <version>1.0-SNAPSHOT</version>
    <dependencies>
        <dependency>
            <groupId>org.apache.flink</groupId>
            <artifactId>flink-scala_2.11</artifactId>
            <version>1.7.2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.apache.flink/flink-streaming-scala -->
        <dependency>
            <groupId>org.apache.flink</groupId>
            <artifactId>flink-streaming-scala_2.11</artifactId>
            <version>1.7.2</version>
        </dependency>
        <dependency>
            <groupId>org.apache.flink</groupId>
            <artifactId>flink-connector-kafka-0.11_2.11</artifactId>
            <version>1.7.2</version>
        </dependency>
        <dependency>
            <groupId>org.apache.bahir</groupId>
            <artifactId>flink-connector-redis_2.11</artifactId>
            <version>1.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.flink</groupId>
            <artifactId>flink-connector-elasticsearch6_2.11</artifactId>
            <version>1.7.2</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.44</version>
        </dependency>
        <dependency>
            <groupId>org.apache.flink</groupId>
            <artifactId>flink-statebackend-rocksdb_2.11</artifactId>
            <version>1.7.2</version>
        </dependency>


    </dependencies>

    <build>
        <plugins>
            <!-- 该插件用于将Scala代码编译成class文件 -->
            <plugin>
                <groupId>net.alchim31.maven</groupId>
                <artifactId>scala-maven-plugin</artifactId>
                <version>3.4.6</version>
                <executions>
                    <execution>
                        <!-- 声明绑定到maven的compile阶段 -->
                        <goals>
                            <goal>testCompile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>3.0.0</version>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
                <executions>
                    <execution>
                        <id>make-assembly</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```

### 2.1.2 添加 scala  框架  和 scala  文件夹

![image-20200730225035809](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20200730225035809.png)

## 2.2 批处理 wordcount

```scala
package com.xdl.wc

import org.apache.flink.api.scala._

/**
  * Project: FlinkTutorial
  * Package: com.atguigu.wc
  * Version: 1.0
  *
 * Created by guoxiaolong on 2020/7/30 14:30
  */

// 批处理代码
object WordCount {
  def main(args: Array[String]): Unit = {
    // 创建一个批处理的执行环境
    val env = ExecutionEnvironment.getExecutionEnvironment

    // 从文件中读取数据
    val inputPath ="D:\\Projects\\BigData\\FlinkTutorial\\src\\main\\resources\\hello.txt"
    val inputDataSet = env.readTextFile(inputPath)

    // 分词之后做count
    val wordCountDataSet = inputDataSet.flatMap(_.split(" "))
      .map( (_, 1) )
      .groupBy(0)
      .sum(1)

    // 打印输出
    wordCountDataSet.print()
  }
}
```

> hello.txt

```txt
hello flink
hello world
hello scala
how are you
fine thank you
and you
```

输出结果

```scala
(thank,1)
(and,1)
(fine,1)
(flink,1)
(world,1)
(are,1)
(scala,1)
(you,3)
(hello,3)
(how,1)

Process finished with exit code 0
```

## 2.3 流处理 wordcount

```scala
package com.xdl.wc

import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.scala._

/**
  * Project: FlinkTutorial
  * Package: com.atguigu.wc
  * Version: 1.0
  *
  * Created by guoxiaolong on 2020/7/30 14:08
  */
object StreamWordCount {
  def main(args: Array[String]): Unit = {

    val params = ParameterTool.fromArgs(args)
    val host: String = params.get("host")
    val port: Int = params.getInt("port")

    // 创建一个流处理的执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    //env.setParallelism(1)
    //env.disableOperatorChaining()

    // 接收socket数据流
    val textDataStream = env.socketTextStream(host, port)

    // 逐一读取数据，分词之后进行wordcount
    val wordCountDataStream = textDataStream.flatMap(_.split("\\s"))
      .filter(_.nonEmpty).startNewChain()
      .map( (_, 1) )
      .keyBy(0)
      .sum(1)

    // 打印输出，并行度设为 1  
    wordCountDataStream.print().setParallelism(1)

    // 执行任务
    env.execute("stream word count job")
  }
}
```

> 在 cmd 窗口 输入

```sh
nc -lk 777
```

![image-20200730225959106](images/image-20200730225959106.png)

> 注意左边的数字为并行度的意思。

![image-20200730230616600](images/image-20200730230616600.png)

# 第 3 章 Flink 部署

## 3.1 Standalone 模式

### 3.1.1 安装

> 解压缩 flink-1.7.2-bin-hadoop27-scala_2.11.tgz，进入 conf 目录中。

![image-20200730231509161](images/image-20200730231509161.png)

![image-20200730231614418](images/image-20200730231614418.png)

> 1 ）修改 flink/conf/flink-conf.yaml  文件

```sh
jobmanager.rpc.address: hadoop102
```

> 执行命令 

```sh
[carlos@hadoop102 flink-1.7.2]$ ./bin/start-cluster.sh
Starting cluster.
Starting standalonesession daemon on host hadoop102.
Starting taskexecutor daemon on host hadoop102.
# 查看进程
[carlos@hadoop102 flink-1.7.2]$ jps
10756 Jps
10648 TaskManagerRunner
```

> 浏览器访问 http://hadoop102:8081

![image-20200730234357555](images/image-20200730234357555.png)

### 3.1.2  提交任务

![image-20200730235049011](images/image-20200730235049011.png)

> 点击提交

![image-20200730235337209](images/image-20200730235337209.png)

## 3.2 Yarn 模式

​		以 Yarn 模式部署 Flink 任务时，要求 Flink 是有 Hadoop 支持的版本，Hadoop 环境需要保证版本在 2.2 以上，并且集群中安装有 HDFS 服务。

1)  启动 hadoop 集群;

2)  启动 yarn-session;

```sh
./yarn-session.sh -n 2 -s 2 -jm 1024 -tm 1024 -nm test -d
```

> 其中：
> -n(--container)：TaskManager 的数量。
> -s(--slots)： 每个 TaskManager 的 slot 数量，默认一个 slot 一个 core，默认每个taskmanager 的 slot 的个			数为 1，有时可以多一些 taskmanager，做冗余。
> -jm：JobManager 的内存（单位 MB)。
> -tm：每个 taskmanager 的内存（单位 MB)。
> -nm：yarn 的 appName(现在 yarn 的 ui 上的名字)。
> -d：后台执行。

![image-20200731000452695](images/image-20200731000452695.png)

3)  执行任务

```sh
./flink  run  -m  yarn-cluster  -c  com.atguigu.flink.app.BatchWcApp
/ext/flink0503-1.0-SNAPSHOT.jar  --input  /applog/flink/input.txt  --output
/applog/flink/output5.csv
```

![image-20200731000522582](images/image-20200731000522582.png)

4)  去 yarn 控制台查看任务状态

![image-20200731000537583](images/image-20200731000537583.png)

## 3.3 Kubernetes 部署

​		容器化部署时目前业界很流行的一项技术，基于 Docker 镜像运行能够让用户更加方便地对应用进行管理和运维。容器管理工具中最为流行的就是 Kubernetes（k8s），而 Flink 也在最近的版本中支持了 k8s 部署模式。

1 ） 搭建 Kubernetes 集群；

2 ） 配置各组件的 yaml 文件；

​		在 k8s 上构建 Flink Session Cluster，需要将 Flink 集群的组件对应的 docker 镜像分别在 k8s 上启动，包括 JobManager、TaskManager、JobManagerService 三个镜像服务。每个镜像服务都可从中央镜像仓库中获取。

3 ）启动 Flink Session Cluster；

```sh
//启动 jobmanager-service  服务
kubectl create -f jobmanager-service.yaml
//启动 jobmanager-deployment 服务
kubectl create -f jobmanager-deployment.yaml
//启动 taskmanager-deployment 服务
kubectl create -f taskmanager-deployment.yaml
```

4 ）访问 Flink UI 页面
集群启动后，就可以通过 JobManagerServicers 中配置的 WebUI 端口，用浏览器输入 url 来访问 Flink UI 页面。

# 第 4 章 Flink 运行架构

## 4.1 Flink 运行时的组件

![image-20200731000929472](images/image-20200731000929472.png)

因为 Flink 是用 Java 和 Scala 实现的，所以所有组件都会运行在 Java 虚拟机上。每个组件的职责：

- 作业管理器（JobManager）
  - 控制一个应用程序执行的主进程，也就是说，每个应用程序都会被一个不同的 JobManager 所控制执行。
  - JobManager 会先接收到要执行的应用程序，这个应用程序会包括：作业图（JobGraph）、逻辑数据流图（logical dataflow graph）和打包了所有的类、库和其它资源的 JAR 包。
  - JobManager 会把 JobGraph 转换成一个物理层面的数据流图，这个图被叫做 “执行图”（ExecutionGraph），包含了所有可以并发执行的任务。
  - JobManager 会向资源管理器（ResourceManager）请求执行任务必要的资源，也就是任务管理器（TaskManager）上的插槽（slot）。一旦它获取到了足够的资源，就会将执行图分发到真正运行它们的 TaskManager 上。而在运行过程中，JobManager 会负责所有需要中央协调的操作，比如说检查点（checkpoints）的协调。
- 任务管理器（TaskManager）
  - Flink 中的工作进程。通常在 Flink 中会有多个 TaskManager 运行，每一个 TaskManager 都包含了一定数量的插槽（slots）。插槽的数量限制了 TaskManager 能够执行的任务数量。
  - 启动之后，TaskManager 会向资源管理器注册它的插槽；收到资源管理器的指令后，TaskManager 就会将一个或多个插槽提供给 JobManager 调用。JobManager 就可以向插槽分配任务（tasks）来执行了。
  - 在执行过程中，一个 TaskManager 可以跟其它运行同一应用程序的 TaskManager 交换数据。
- 资源管理器（ResourceManager）
  - 主要负责管理任务管理器（TaskManager）的插槽（slot），TaskManger 插槽是 Flink 中 定义的处理资源单元。
  - Flink 为不同的环境和资源管理工具提供了不同资源管理器，比如 YARN、Mesos、K8s，以及 standalone 部署。
  - 当 JobManager 申请插槽资源时，ResourceManager 会将有空闲插槽的 TaskManager 分配给JobManager。如果 ResourceManager 没有足够的插槽来满足 JobManager 的请求，它还可以向资源提供平台发起会话，以提供启动 TaskManager 进程的容器。另外，ResourceManager 还负责终止空闲的 TaskManager，释放计算资源。
- 分发器（Dispatcher）
  - 可以跨作业运行，它为应用提交提供了 REST 接口。
  - 当一个应用被提交执行时，分发器就会启动并将应用移交给一个 JobManager。
  - Dispatcher 也会启动一个 Web UI，用来方便地展示和监控作业执行的信息。
  - Dispatcher 在架构中可能并不是必需的，这取决于应用提交运行的方式。

## 4.2 任务提交流程

![image-20200731001909802](images/image-20200731001909802.png)

​		上图是从一个较为高层级的视角，来看应用中各组件的交互协作。如果部署的集群环境不同（例如 YARN，Mesos，Kubernetes，standalone 等），其中一些步骤可以被省略，或是有些组件会运行在同一个 JVM 进程中。具体地，如果我们将 Flink 集群部署到 YARN 上，那么就会有如下的提交流程：

![image-20200731002126912](images/image-20200731002126912.png)

​		Flink 任务提交后，Client 向 HDFS 上传 Flink 的 Jar 包和配置，之后向 Yarn ResourceManager 提交任务，ResourceManager 分配 Container 资源并通知对应的 NodeManager 启动 ApplicationMaster ApplicationMaster 启动后加载 Flink 的 Jar 包和配置构建环境，然后启动 JobManager，之后 ApplicationMaster 向 ResourceManager 申 请 资 源 启 动 TaskManager ， ResourceManager 分 配 Container 资 源 后 ， 由 ApplicationMaster 通 知 资 源 所 在 节 点 的 NodeManager 启 动 TaskManager ，NodeManager 加载 Flink 的 Jar 包和配置构建环境并启动 TaskManager，TaskManager 启动后向 JobManager 发送心跳包，并等待 JobManager 向其分配任务。

## 4.3 任务调度原理

![image-20200731002317537](images/image-20200731002317537.png)

​		客 户 端 不 是 运 行 时 和 程 序 执 行 的 一 部 分 ， 但 它 用 于 准 备 并 发 送 dataflow(JobGraph)给 Master(JobManager)，然后，客户端断开连接或者维持连接以等待接收计算结果。

​		当 Flink 集 群 启 动 后 ， 首 先 会 启 动 一 个 JobManger 和 一 个 或 多 个 的 TaskManager。由 Client 提交任务给 JobManager，JobManager 再调度任务到各个 TaskManager 去执行，然后 TaskManager 将心跳和统计信息汇报给 JobManager。

​		TaskManager 之间以流的形式进行数据的传输。上述三者均为独立的 JVM 进程。

​		Client 为提交 Job 的客户端，可以是运行在任何机器上（与 JobManager 环境连通即可）。提交 Job 后，Client 可以结束进程（Streaming 的任务），也可以不结束并等待结果返回。

​		JobManager 主要负责调度 Job 并协调 Task 做 checkpoint，职责上很像 Storm 的 Nimbus。从 Client 处接收到 Job 和 JAR 包等资源后，会生成优化后的执行计划，并以 Task 的单元调度到各个 TaskManager 去执行。

​		TaskManager 在启动的时候就设置好了槽位数（Slot），每个 slot 能启动一个 Task，Task 为线程。从 JobManager 处接收需要部署的 Task，部署启动后，与自己的上游建立 Netty 连接，接收数据并处理。

### 4.3.1 TaskManger 与 Slots

![image-20200731002545155](images/image-20200731002545155.png)

- Flink 中每一个 worker(TaskManager) 都是一个 JVM  进程，它可能会在 独立的线程上执行一个或多个 subtask。

- 为了控制一个 worker 能接收多少个 task，worker 通过 task slot 来进行控制（一个 worker 至少有一个 task slot）。

- 每个 task slot 表示 TaskManager 拥有资源的 一个固定大小的子集。假如一个TaskManager 有三个 slot，那么它会将其管理的内存分成三份给各个 slot。

- 资源 slot 化意味着一个 subtask 将不需要跟来自其他 job 的 subtask 竞争被管理的内存，取而代之的是它将拥有一定数量的内存储备。需要注意的是，这里不会涉及到 CPU 的隔离，slot 目前仅仅用来隔离 task 的受管理的内存。

- 通过调整 task slot 的数量，允许用户定义 subtask 之间如何互相隔离。如果一个 TaskManager 一个 slot，那将意味着每个 task group 运行在独立的 JVM 中（该 JVM 可能是通过一个特定的容器启动的），而一个 TaskManager 多个 slot 意味着更多的 subtask 可以共享同一个 JVM。而在同一个 JVM 进程中的 task 将共享 TCP 连接（基于多路复用）和心跳消息。它们也可能共享数据集和数据结构，因此这减少了每个 task 的负载。

  > 子任务共享 Slot

![image-20200731002743372](images/image-20200731002743372.png)

- 默认情况下，Flink 允许子任务共享 slot，即使它们是不同任务的子任务（前提是它们来自同一个 job）。 这样的结果是，一个 slot 可以保存作业的整个管道。
- Task Slot 指 是静态的概念，是指 TaskManager  具有的并发执行能力，可以通过参数 taskmanager.numberOfTaskSlots 进行配置；
- 而 并行度 parallelism  是动态概念，即 TaskManager  运行程序时实际使用的并发能力，可以通过参数 parallelism.default进行配置。
- 也就是说，假设一共有 3 个 TaskManager，每一个 TaskManager 中的分配 3 个 TaskSlot，也就是每个 TaskManager 可以接收 3 个 task，一共 9 个 TaskSlot，如果我们设置 parallelism.default=1，即运行程序默认的并行度为 1，9 个 TaskSlot 只用了 1个，有 8 个空闲，因此，设置合适的并行度才能提高效率。

![image-20200731003108084](images/image-20200731003108084.png)

![image-20200731003128168](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20200731003128168.png)

### 4.3.2  程序与数据流（DataFlow）

![image-20200731003226685](images/image-20200731003226685.png)

- 所有的 Flink 程序都是由三部分组成的： Source 、Transformation 和 Sink。
- Source 负责读取数据源，Transformation 利用各种算子进行处理加工，Sink 负责输出。
- 在运行时，Flink 上运行的程序会被映射成“逻辑数据流”（dataflows），它包含了这三部分。
-  每一个 dataflow 个 以一个或多个 sources 开始以一个或多个 sinks 结束。dataflow 类似于任意的有向无环图（DAG）。
- 在大部分情况下，程序中的转换运算（transformations）跟 dataflow 中的算子（operator）是一一对应的关系，但有时候，一个 transformation 可能对应多个 operator。

> 程序与数据流

![image-20200731003456150](images/image-20200731003456150.png)

### 4.3.3  执行图 （ExecutionGraph）

​		由 Flink 程序直接映射成的数据流图是 StreamGraph，也被称为逻辑流图，因为它们表示的是计算逻辑的高级视图。为了执行一个流处理程序，Flink 需要将逻辑流图转换为物理数据流图（也叫执行图），详细说明程序的执行方式。

- Flink 中的执行图可以分成四层：StreamGraph -> JobGraph -> ExecutionGraph -> 物理执行图。
- StreamGraph：是根据用户通过 Stream API 编写的代码生成的最初的图。用来表示程序的拓扑结构。
- JobGraph：StreamGraph 经过优化后生成了 JobGraph，提交给 JobManager 的数据结构。主要优化为，将多个符合条件的节点 chain 在一起作为一个节点，这样可以减少数据在节点之间流动所需要的序列化/反序列化/传输消耗。
- ExecutionGraph ： JobManager 根 据 JobGraph 生 成 ExecutionGraph 。ExecutionGraph 是 JobGraph 的并行化版本，是调度层最核心的数据结构。
- 物理执行图：JobManager 根据 ExecutionGraph 对 Job 进行调度后，在各个 TaskManager 上部署 Task 后形成的 “图”，并不是一个具体的数据结构。

![image-20200731003808117](images/image-20200731003808117.png)

### 4.3.4  并行度 （Parallelism）

​	Flink 程序的执行具有 并行、分布式的特性。在执行过程中，一个流（stream）包含一个或多个分区（stream partition），而每一个算子（operator）可以包含一个或多个子任务（operator subtask），这些子任务在不同的线程、不同的物理机或不同的容器中彼此互不依赖地执行。

> 并行数据流

![image-20200731003912143](images/image-20200731003912143.png)

- 一个特定算子的子任务（subtask）的个数被称之其为并行度（ parallelism ）。一般情况下，一个流程序的并行度，可以认为就是其所有算子中最大的并行度。

![image-20200731004208673](images/image-20200731004208673.png)

- 一个程序中，不同的算子可能具有不同的并行度。
- Stream 在算子之间传输数据的形式可以是 one-to-one(forwarding)的模式也可以是 redistributing 的模式，具体是哪一种形式，取决于算子的种类。
- One-to-one：stream(比如在 source 和 map operator 之间)维护着分区以及元素的顺序。那意味着 map 算子的子任务看到的元素的个数以及顺序跟 source 算子的子任务生产的元素的个数、顺序相同，map、fliter、flatMap 等算子都是 one-to-one 的对应关系。
- Redistributing：stream(map()跟 keyBy/window 之间或者 keyBy/window 跟 sink 之间)的分区会发生改变。每一个算子的子任务依据所选择的 transformation 发送数据到不同的目标任务。例如，keyBy() 基于 hashCode 重分区、broadcast 和 rebalance 会随机重新分区，这些算子都会引起 redistribute 过程，而 redistribute 过程就类似于Spark 中的 shuffle 过程。类似于 spark 中的窄依赖、宽依赖。

### 4.3.5  任务链（Operator Chains )

> task 与 operator chains

![image-20200731004540331](images/image-20200731004540331.png)

- Flink 采用了一种称为任务链的优化技术，可以在特定条件下减少本地通信的开销。为了满足任务链的要求，必须将两个或多个算子设为相同的并行度，并通过本地转发（local forward）的方式连接。
- 相同并行度的 one to one  操作，Flink 这样相连的算子链接在一起形成一个 task，原来的算子成为里面的 subtask。
- 并行度相同、并且是 One-to-One 操作，两个条件缺一不可。
- 将算子链接成 task 是非常有效的优化：它能减少线程之间的切换和基于缓存区的数据交换，在减少时延的同时提升吞吐量。链接的行为可以在编程 API 中进行指定。 

# 第 5 章 Flink 流处理 API

![image-20200731005451855](images/image-20200731005451855.png)

## 5.1 Environment

### 5.1.1 getExecutionEnvironment

​		创建一个执行环境，表示当前执行程序的上下文。 如果程序是独立调用的，则此方法返回本地执行环境；如果从命令行客户端调用程序以提交到集群，则此方法返回此集群的执行环境，getExecutionEnvironment 会根据查询运行的方式决定返回什么样的运行环境，==最常用的一种创建执行环境的方式==。

```scala
val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
```

```scala
val env = StreamExecutionEnvironment.getExecutionEnvironment
```

> 如果没有设置并行度，会以 flink-conf.yaml 中的配置为准，默认是 1

![image-20200731005715905](images/image-20200731005715905.png)

### 5.1.2 createLocalEnvironment

> 返回本地执行环境，需要在调用时指定默认的并行度。

```scala
val env = StreamExecutionEnvironment.createLocalEnvironment(1)
```

### 5.1.3 createRemoteEnvironment

> 返回集群执行环境，将 Jar 提交到远程服务器。需要在调用时指定 JobManager 的 IP 和端口号，并指定要在集群中运行的 Jar 包。

```scala
val env = ExecutionEnvironment.createRemoteEnvironment("jobmanage-hostname",
6123,"YOURPATH//wordcount.jar")
```

## 5.2 Source

### 5.2.1  从集合读取数据

```scala
//  定义样例类，传感器 id ，时间戳，温度
case class SensorReading(id: String, timestamp: Long, temperature: Double)
object Sensor {
    def main(args: Array[String]): Unit = {
        val env = StreamExecutionEnvironment.getExecutionEnvironment
        val stream1 = env
          .fromCollection(List(
            SensorReading("sensor_1", 1547718199, 35.80018327300259),
            SensorReading("sensor_6", 1547718201, 15.402984393403084),
            SensorReading("sensor_7", 1547718202, 6.720945201171228),
            SensorReading("sensor_10", 1547718205, 38.101067604893444)
          ))
        stream1.print("stream1:").setParallelism(1)
        env.execute()
     }
}
```

### 5.2.2  从文件读取数据

```scala
val stream2 = env.readTextFile("YOUR_FILE_PATH")
```

### 5.2.3 以 kafka  消息队列的数据作为来源

> 需要引入 kafka 连接器的依赖 pom.xml

```xml
<!--https://mvnrepository.com/artifact/org.apache.flink/flink-connector-kafka-0.11 -->
< dependency>
	< groupId>org.apache.flink</ groupId>
	< art ifactId>flink-connector-kafka-0.11_2.11</ artifactId>
	< version>1.7.2</ version>
</dependency>
```

> 代码

```scala
val properties = new Properties()
properties.setProperty("bootstrap.servers", "localhost:9092")
properties.setProperty("group.id", "consumer-group")
properties.setProperty("key.deserializer",
"org.apache.kafka.common.serialization.StringDeserializer")
properties.setProperty("value.deserializer",
"org.apache.kafka.common.serialization.StringDeserializer")
properties.setProperty("auto.offset.reset", "latest")
val stream3 = env.addSource(new FlinkKafkaConsumer011[String]("sensor", new
SimpleStringSchema(), properties))
```

![image-20200731011227446](images/image-20200731011227446.png)

### 5.2.4  自定义 Source

除了以上的 source 数据来源，我们还可以自定义 source。需要做的，只是传入一个 SourceFunction 就可以。

```scala
val stream4 = env.addSource( new MySensorSource() )
```

我们希望可以随机生成传感器数据，MySensorSource 具体的代码实现

```scala
class SensorSource() extends SourceFunction[SensorReading] {
  //定义一个 flag，表示数据源是否正确
  var running: Boolean = true
    
  //取消数据源的生成
  override def cancel(): Unit = {
    running = false
  }
  //正常生成数据
  override def run(sourceContext: SourceFunction.SourceContext[SensorReading]): Unit = {
    //初始化一个随机发生器
    var rand = new Random()
    //初始化定义一组传感器温度数据
    var curTemp = 1.to(10).map(
      i => ("sensor_" + i, 60 + rand.nextGaussian() * 20)
    )

    //用无线循环，产生数据流
    while (running) {
      //在前一次温度的基础上更新温度值
      curTemp = curTemp.map(
        t => (t._1, t._2 + rand.nextGaussian())
      )
      //获取当前时间戳
      var curTime = System.currentTimeMillis();
      curTemp.foreach(
        t => sourceContext.collect( SensorReading(t._1, curTime, t._2) )
      )
      //设置时间间隔
      Thread.sleep(500)
    }
  }
}
```

```scala
# 输出结果
stream4> SensorReading(sensor_1,1596166237625,58.52881273862491)
stream4> SensorReading(sensor_2,1596166237625,47.78707581823334)
stream4> SensorReading(sensor_3,1596166237625,22.856453856137716)
stream4> SensorReading(sensor_4,1596166237625,83.80363900855411)
stream4> SensorReading(sensor_5,1596166237625,37.96874274545165)
stream4> SensorReading(sensor_6,1596166237625,43.60960402520461)
stream4> SensorReading(sensor_7,1596166237625,30.502429174217355)
stream4> SensorReading(sensor_8,1596166237625,68.64423293349529)
stream4> SensorReading(sensor_9,1596166237625,54.04086508109228)
stream4> SensorReading(sensor_10,1596166237625,77.4414929371223)
stream4> SensorReading(sensor_1,1596166238127,57.0486780293933)
...
```

### 5.3 Transform

> 转换算子

### 5.3.1 map

![image-20200731113346041](images/image-20200731113346041.png)

```scala
val streamMap = stream.map { x => x * 2 }
```



### 5.3.2  flatMap

flatMap 的函数签名：def flatMap[A,B](as: List[A])(f: A ⇒ List[B]): List[B]

例如: flatMap(List(1,2,3))(i ⇒ List(i,i))，结果是 List(1,1,2,2,3,3),

而 List("a b", "c d").flatMap(line ⇒ line.split(" "))，结果是 List(a, b, c, d)。

```scala
val streamFlatMap = stream.flatMap{
	x => x.split(" ")
}
```

### 5.3.3 Filter

![image-20200731114044383](images/image-20200731114044383.png)

```scala
val streamFilter = stream.filter{
	x => x == 1
}
```

### 5.3.4 KeyBy

![image-20200731114128992](images/image-20200731114128992.png)

**DataStream**  →  **KeyedStream**：逻辑地将一个流拆分成不相交的分区，每个分区包含具有相同 key 的元素，在内部以 hash 的形式实现的。

```scala
val aggStream = dataStream.keyBy("id")
```

### 5.3.5  滚动聚合算子（Rolling Aggregation ）

这些算子可以针对 KeyedStream 的每一个支流做聚合。

- sum()
- min()
- max()
- minBy()
- maxBy()

### 5.3.6 Reduce

​        **KeyedStream**  →  **DataStream**：一个分组数据流的聚合操作，合并当前的元素和上次聚合的结果，产生一个新的值，返回的流中包含每一次聚合的结果，而不是只返回最后一次聚合的最终结果。

```scala
object TransformTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    //读入数据
    var streamFromFile = env.readTextFile("D:\\Projects\\BigData\\FlinkTutorial\\src\\main\\resources\\sensor.txt")
    //1.聚合操作
    val aggStream = dataStream.keyBy("id")
      
    //.sum("temperature")
    //输出当前传感器最新的温度 +10, 而时间戳就是上一次数据的时间 + 1
      .reduce((x, y) => SensorReading(x.id, x.timestamp + 1, y.temperature + 10))

    //输出数据
    dataStream.print()
  
    env.execute("trasnferm test")
  }
}
```

> 源数据 sensor.txt

```text
sensor_1, 1547718199, 35.80018327300259
sensor_6, 1547718201, 15.402984393403084
sensor_7, 1547718202, 6.720945201171228
sensor_10, 1547718205, 38.101067604893444
sensor_1, 1547718206, 35.1
sensor_1, 1547718207, 35.6
```

### 5.3.7 Split  和 Select

> Split

![image-20200731121214959](images/image-20200731121214959.png)

**DataStream**  →  **SplitStream**：根据某些特征把一个 DataStream 拆分成两个或者多个 DataStream。

> Select

![image-20200731121304492](images/image-20200731121304492.png)

**SplitStream**  → **DataStream**：从一个 SplitStream中获取一个或者多个 DataStream。
需求：传感器数据按照温度高低（以 30 度为界），拆分成两个流。

```scala
object TransformTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    //读入数据
    var streamFromFile = env.readTextFile("D:\\Projects\\BigData\\FlinkTutorial\\src\\main\\resources\\sensor.txt")

    //基本转换算子和简单聚合算子
    val dataStream: DataStream[SensorReading] = streamFromFile.map(data => {
      val dataArray = data.split(",")
      SensorReading(dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })
      
    //多流转换算子 DataStream → SplitStream
    //传感器数据按照温度高低（以 30 度为界），拆分成两个流
    val splitStream = dataStream.split(data => {
      if (data.temperature > 30) Seq("high") else Seq("low")
    })

    val highTempStrem = splitStream.select("high")
    val lowTempStrem = splitStream.select("low")
    val allTempStrem = splitStream.select("high", "low")
      
    //输出数据
    highTempStrem.print("high")
    lowTempStrem.print("low")
    allTempStrem.print("all")
      
    env.execute("trasnferm test")
  }
}
```



### 5.3.8 Connect  和 CoMap

> Connect 算子

![image-20200731122016599](images/image-20200731122016599.png)

**DataStream,DataStream**  →  **ConnectedStreams**：连接两个保持他们类型的数据流，两个数据流被 Connect 之后，只是被放在了一个同一个流中，内部依然保持各自的数据和形式不发生任何变化，两个流相互独立。

> CoMap,CoFlatMap

![image-20200731122120153](images/image-20200731122120153.png)

**ConnectedStreams** → **DataStream**：作用于 ConnectedStreams 上，功能与 map 和 flatMap 一样，对 ConnectedStreams 中的每一个 Stream 分别进行 map 和 flatMap 处理。

```scala
object TransformTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    //读入数据
    var streamFromFile = env.readTextFile("D:\\Projects\\BigData\\FlinkTutorial\\src\\main\\resources\\sensor.txt")

    //基本转换算子和简单聚合算子
    val dataStream: DataStream[SensorReading] = streamFromFile.map(data => {
      val dataArray = data.split(",")
      SensorReading(dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
    })
      
    //多流转换算子
    val splitStream = dataStream.split(data => {
      if (data.temperature > 30) Seq("high") else Seq("low")
    })

    val highTempStrem = splitStream.select("high")
    val lowTempStrem = splitStream.select("low")
    val allTempStrem = splitStream.select("high", "low")

    //3.合并两条流 ConnectedStreams → DataStream
    val warningStream = highTempStrem.map(data => (data.id, data.temperature))
    val connectedStreams = warningStream.connect(lowTempStrem)

    val coMapStream = connectedStreams.map(
      warningData => (warningData._1, warningData._2, "high temperature warning"),
      lowData => (lowData.id, "healthy")
    )
  
    //输出数据
    coMapDataStream.print("coMap")
      
    env.execute("trasnferm test")
  }
}
```

### 5.3.9 Union

![image-20200731124331916](images/image-20200731124331916.png)

**DataStream**  →  **DataStream**：对两个或者两个以上的 DataStream 进行 union 操作，产生一个包含所有 DataStream 元素的新 DataStream。

```scala
val unionStream = highTempStrem.union(lowTempStrem)
```

> Connect  与 Union  区别

1. Union 之前两个流的类型必须是一样，Connect 可以不一样，在之后的 coMap 中再去调整成为一样的。
2. Connect 只能操作两个流，Union 可以操作多个。

## 5.4 支持的数据类型

​		Flink 流应用程序处理的是以数据对象表示的事件流。所以在 Flink 内部，我们需要能够处理这些对象。它们需要被序列化和反序列化，以便通过网络传送它们；或者从状态后端、检查点和保存点读取它们。为了有效地做到这一点，Flink 需要明确知道应用程序所处理的数据类型。Flink 使用类型信息的概念来表示数据类型，并为每个数据类型生成特定的序列化器、反序列化器和比较器。

​		Flink 还具有一个类型提取系统，该系统分析函数的输入和返回类型，以自动获取类型信息，从而获得序列化器和反序列化器。但是，在某些情况下，例如 lambda 函数或泛型类型，需要显式地提供类型信息，才能使应用程序正常工作或提高其性能。

​		Flink 支持 Java 和 Scala 中所有常见数据类型。使用最广泛的类型有以下几种。

### 5.4.1  基础数据类型

Flink 支持所有的 Java 和 Scala 基础数据类型，Int, Double, Long, String, …

```scala
val numbers: DataStream[Long] = env.fromElements(1L, 2L, 3L, 4L)
numbers.map( n => n + 1 )
```

### 5.4.2 Java 和 Scala  元组（Tuples ）

```scala
val persons: DataStream[(String, Integer)] = env.fromElements(
	("Adam", 17),
	("Sarah", 23) )
persons.filter(p => p._2 > 18)
```

### 5.4.3 Scala  样例类（case classes ）

```scala
case class Person(name: String, age: Int)
val persons: DataStream[Person] = env.fromElements(
    Person("Adam", 17),
    Person("Sarah", 23) )
persons.filter(p => p.age > 18)
```

### 5.4.4 Java  简单对象（POJOs ）

```java
public class Person {
    public String name;
    public int age;
    
    public Person() {}
    
    public Person(String name, int age) {
    this.name = name;
    this.age = age;
    }
}
DataStream<Person> persons = env.fromElements(
    new Person("Alex", 42),
    new Person("Wendy", 23));
```



### 5.4.5  其它（Arrays, Lists, Maps, Enums,  等等）

​		Flink 对 Java 和 Scala 中的一些特殊目的的类型也都是支持的，比如 Java 的 ArrayList，HashMap，Enum 等等。



## 5.5 实现 UDF 函数——更细粒度的控制流

### 5.5.1  函数类（Function Classes ）

Flink 暴露了所有 udf 函数的接口(实现方式为接口或者抽象类)。例如 MapFunction, FilterFunction, ProcessFunction 等等。

> 实现  FilterFunction 接口

```scala
class FilterFilter extends FilterFunction[String] {
    override def filter(value: String): Boolean = {
        value.contains("flink")
        }
    }
val flinkTweets = tweets.filter(new FlinkFilter)
```

> 还可以将函数实现成匿名类

```scala
val flinkTweets = tweets.filter(
    new RichFilterFunction[String] {
        override def filter(value: String): Boolean = {
        value.contains("flink")
        }
    }
)
```

> 我们 filter 的字符串"flink"还可以当作参数传进去。

```scala
val tweets: DataStream[String] = ...
val flinkTweets = tweets.filter(new KeywordFilter("flink"))

class KeywordFilter(keyWord: String) extends FilterFunction[String] {
    override def filter(value: String): Boolean = {
        value.contains(keyWord)
    }
}
```

### 5.5.2  匿名函数（Lambda Functions ）

```scala
val tweets: DataStream[String] = ...
val flinkTweets = tweets.filter(_.contains("flink"))
```

```scala
// _ .id 代表 data => data.id
dataStream.filter( _.id.statWith("sensor_1")).print
```

### 5.5.3  富函数（Rich Functions ）

​	   “富函数” 是 DataStream API 提供的一个函数类的接口，所有 Flink 函数类都有其 Rich 版本。它与常规函数的不同在于，==可以获取运行环境的上下文，并拥有一些生命周期方法==，所以可以实现更复杂的功能。

- RichMapFunction
- RichFlatMapFunction
- RichFilterFunction
- ...

Rich Function 有一个生命周期的概念。==典型的生命周期方法==有

- open() 方法是 rich function 的初始化方法，当一个算子例如 map 或者 filter 被调用之前 open()会被调用。
- close() 方法是生命周期中的最后一个调用的方法，做一些清理工作。
- getRuntimeContext() 方法提供了函数的 RuntimeContext 的一些信息，例如函数执行的并行度，任务的名字，以及 state 状态

```scala
class MyFlatMap extends RichFlatMapFunction[Int, (Int, Int)] {
    var subTaskIndex = 0
    
    override def open(configuration: Configuration): Unit = {
        subTaskIndex = getRuntimeContext.getIndexOfThisSubtask
        //  以下可以做一些初始化工作 ， 例如建立一个和 HDFS 的连接
    }
    
    override def flatMap(in: Int, out: Collector[(Int, Int)]): Unit = {
        if (in % 2 == subTaskIndex) {
        	out.collect((subTaskIndex, in))
        }
    }
    override def close(): Unit = {
    //  以下做一些清理工作，例如断开和 HDFS 的连接。
    }
}
```

## 5.6 Sink

​		Flink 没有类似于 spark 中 foreach 方法，让用户进行迭代的操作。虽有对外的输出操作都要利用 Sink 完成。最后通过类似如下方式完成整个任务最终输出操作。

```scala
stream.addSink(new MySink(xxxx))
```

官方提供了一部分的框架的 sink。除此以外，需要用户自定义实现 sink。

![image-20200731133910952](images/image-20200731133910952.png)

![image-20200731133921297](images/image-20200731133921297.png)

### 5.6.1 Kafka

> pom.xml

```xml
< dependency>
    < groupId>org.apache.flink</ groupId>
    < artifactId>flink-connector-kafka-0.11_2.11</ artifactId>
    < version>1.7.2</ version>
</dependency>
```

> 主函数中添加 sink

```scala
object KafkaSinkTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    //source
    //val inputStream = env.readTextFile("D:\\Projects\\BigData\\FlinkTutorial\\src\\main\\resources\\sensor.txt")

    //从 Kafka 中读取数据
    //创建 Kafka 相关的配置
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "localhost:9092")
    properties.setProperty("group.id", "consumer-group")
    properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty("auto.offset.reset", "latest")

    val inputStream = env.addSource(new FlinkKafkaConsumer011[String]("sensor", new SimpleStringSchema(), properties))

    // Transform操作
    val dataStream = inputStream
      .map(
        data => {
          val dataArray = data.split(",")
          SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble ).toString  // 转成String方便序列化输出
        }
      )

    //sink
    dataStream.addSink( new FlinkKafkaProducer011[String]( "sinkTest", new SimpleStringSchema(), properties) )
    dataStream.print()

    env.execute("kafka sink test")
  }
}
```

> 启动 Kafka

```sh
# 启动生产者
[carlos@hadoop102 kafka_2.11-2.1.0]$ ./bin/kafka-console-producer.sh --broker-list localhost:9092 --topic sensor

# 启动消费者
[carlos@hadoop102 kafka_2.11-2.1.0]$ ./bin/kafka-console-consumer.sh --bootstrap-sever localhost:9092 --topic sinkTest
```

![image-20200731134817662](images/image-20200731134817662.png)

### 5.6.2 Redis

> pom.xml

```xml
<!-- https://mvnrepository.com/artifact/org.apache.bahir/flink-connector-redis
-->
< dependency>
    < groupId>org.apache.bahir</ groupId>
    < artifactId>flink-connector-redis_2.11</ artifactId>
    < version>1.0</ version>
</dependency>
```

定义一个 redis 的 mapper 类，用于定义保存到 redis 时调用的命令

```scala
object RedisSinkTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    // source
    val inputStream = env.readTextFile("D:\\Projects\\BigData\\FlinkTutorial\\src\\main\\resources\\sensor.txt")

    // transform
    val dataStream = inputStream
      .map(
        data => {
          val dataArray = data.split(",")
          SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble )
        }
      )

    val conf = new FlinkJedisPoolConfig.Builder()
      .setHost("localhost")
      .setPort(6379)
      .build()

    // sink
    dataStream.addSink( new RedisSink(conf, new MyRedisMapper()) )

    env.execute("redis sink test")
  }
}

class MyRedisMapper() extends RedisMapper[SensorReading]{

  // 定义保存数据到redis的命令
  override def getCommandDescription: RedisCommandDescription = {

    // 把传感器id和温度值保存成哈希表 HSET key field value
    new RedisCommandDescription( RedisCommand.HSET, "sensor_temperature" )
  }

  // 定义保存到redis的value
  override def getValueFromData(t: SensorReading): String = t.temperature.toString

  // 定义保存到redis的key
  override def getKeyFromData(t: SensorReading): String = t.id
}
```

### 5.6.3 Elasticsearch

> pom.xml

```xml
<dependency>
    < groupId>org.apache.flink</ groupId>
    < artifactId>flink-connector-elasticsearch6_2.11</ artifactId>
    < version>1.7.2</ version>
</dependency>
```

> 在主函数中调用

```scala
object EsSinkTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    // source
    val inputStream = env.readTextFile("D:\\Projects\\BigData\\FlinkTutorial\\src\\main\\resources\\sensor.txt")

    // transform
    val dataStream = inputStream
      .map(
        data => {
          val dataArray = data.split(",")
          SensorReading( dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble )
        }
      )

    val httpHosts = new util.ArrayList[HttpHost]()
    httpHosts.add(new HttpHost("localhost", 9200))

    // 创建一个esSink 的builder
    val esSinkBuilder = new ElasticsearchSink.Builder[SensorReading](
      httpHosts,
      new ElasticsearchSinkFunction[SensorReading] {
        override def process(element: SensorReading, ctx: RuntimeContext, indexer: RequestIndexer): Unit = {
          println("saving data: " + element)

          // 包装成一个 Map 或者 JsonObject
          val json = new util.HashMap[String, String]()
          json.put("sensor_id", element.id)
          json.put("temperature", element.temperature.toString)
          json.put("ts", element.timestamp.toString)

          // 创建index request，准备发送数据
          val indexRequest = Requests.indexRequest()
            .index("sensor")
            .`type`("readingdata")
            .source(json)

          // 利用index发送请求，写入数据
          indexer.add(indexRequest)
          println("data saved.")
        }
      }
    )

    // sink
    dataStream.addSink( esSinkBuilder.build() )

    env.execute("es sink test")
  }
}
```

> 启动 ElasticSearch

```sh
[carlos@hadoop102 Elasticsearch-5.6.2]$ ./bin/elasticsearch
```

![image-20200731141532465](images/image-20200731141532465.png)

### 5.6.4 JDBC  自定义 sink

> pom.xml

```xml
<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.44< version>
</dependency>
```

添加 MyJdbcSink

```scala
object JdbcSinkTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    // source
    val inputStream = env.readTextFile("D:\\Projects\\BigData\\FlinkTutorial\\src\\main\\resources\\sensor.txt")

    // transform
    val dataStream = inputStream
      .map(
        data => {
          val dataArray = data.split(",")
          SensorReading(dataArray(0).trim, dataArray(1).trim.toLong, dataArray(2).trim.toDouble)
        }
      )

    // sink
    // 在 main 方法中增加，把明细保存到 mysql 中
    dataStream.addSink( new MyJdbcSink() )

    env.execute("jdbc sink test")
  }
}

class MyJdbcSink() extends RichSinkFunction[SensorReading]{
  // 定义sql连接、预编译器
  var conn: Connection = _
  var insertStmt: PreparedStatement = _
  var updateStmt: PreparedStatement = _

  // 初始化，创建连接和预编译语句
  override def open(parameters: Configuration): Unit = {
    super.open(parameters)
    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root")
    insertStmt = conn.prepareStatement("INSERT INTO temperatures (sensor, temp) VALUES (?,?)")
    updateStmt = conn.prepareStatement("UPDATE temperatures SET temp = ? WHERE sensor = ?")
  }

  // 调用连接，执行sql
  override def invoke(value: SensorReading, context: SinkFunction.Context[_]): Unit = {
    // 执行更新语句
    updateStmt.setDouble(1, value.temperature)
    updateStmt.setString(2, value.id)
    updateStmt.execute()

    // 如果update没有查到数据，那么执行插入语句
    if( updateStmt.getUpdateCount == 0 ){
      insertStmt.setString(1, value.id)
      insertStmt.setDouble(2, value.temperature)
      insertStmt.execute()
    }
  }

  // 关闭时做清理工作
  override def close(): Unit = {
    insertStmt.close()
    updateStmt.close()
    conn.close()
  }
}
```

启动 MySQL，创建 test 表并插入数据

# 第 6 章 Flink 中的 Window

## 6.1 窗口（Window）

### 6.1.1 Window  概述

![image-20200730172021758](images/image-20200730172021758-1597711566779.png)

- 一般真实的流都是无界的，怎样处理无界的数据？
- 可以把无限的数据流进行切分，得到有限的数据集进行处理——也就是得到有界流
- 窗口（Window）就是将无限流切割为有限流的一种方式，它会将流数据分发到有限大小的通（bucket）中进行分析

### 6.1.2 Window  类型

- 时间窗口（Time Window）

  - 滚动事件窗口（Tumbling Windows）

    - 将数据依据固定的窗口长度对数据进行切分。

    - 时间堆积，窗口长度固定，没有重叠。

      滚动窗口分配器将每个元素分配到一个指定窗口大小的窗口中，滚动窗口有一个固定的大小，并且不会出现重叠。例如：如果你指定了一个 5 分钟大小的滚动窗口，窗口的创建如下图所示

  ![image-20200731143220205](images/image-20200731143220205.png)

  适用场景：适合做 BI 统计等（做每个时间段的聚合计算）

  - 滑动事件窗口

    - 滑动窗口是固定窗口的更广义的一种形式，滑动窗口由固定的窗口长度和滑动间隔组成。
    - 窗口长度固定，可以有重叠。

    滑动窗口分配器将元素分配到固定长度的窗口中，与滚动窗口类似，窗口的大小由窗口大小参数来配置，另一个窗口滑动参数控制滑动窗口开始的频率。因此，滑动窗口如果滑动参数小于窗口大小的话，窗口是可以重叠的，在这种情况下元素会被分配到多个窗口中。

    例如，你有 10 分钟的窗口和 5 分钟的滑动，那么每个窗口中 5 分钟的窗口里包含着上个 10 分钟产生的数据，如下图所示

  ![image-20200731143511078](images/image-20200731143511078.png)

  适用场景：对最近一个时间段内的统计（求某接口最近 5min 的失败率来决定是否要报警）。

  - 会话窗口

    - 由一系列事件组合一个指定时间长度的 timeout 间隙组成，类似于 web 应用的
      session，也就是一段时间没有接收到新数据就会生成新的窗口

    - 特点：时间无对齐

      session 窗口分配器通过 session 活动来对元素进行分组，session 窗口跟滚动窗口和滑动窗口相比，不会有重叠和固定的开始时间和结束时间的情况，相反，当它在一个固定的时间周期内不再收到元素，即非活动间隔产生，那个这个窗口就会关闭。一个 session 窗口通过一个 session 间隔来配置，这个 session 间隔定义了非活跃周期的长度，当这个非活跃周期产生，那么当前的 session 将关闭并且后续的元素将被分配到新的 session 窗口中去。

  ![image-20200731144653728](images/image-20200731144653728.png)

- 计数窗口（Count Window）

  - 滚动计数窗口
  - 滑动计数窗口

## 6.2 Window API

### 6.2.1 窗口分配器

- 窗口分配器——window() 方法
  - 我们可以用 .window() 来定义一个窗口，然后基于这个 window 去做一些聚合或者其他处理操作。注意 ==window() 方法必须在 keyBy 之后才能用==。
  - Flink 提供了更加简单的 .timeWindow 和 .countWindow 方法，用于定义时间窗口和计数窗口。

```scala
val minTempPerWindow = dataStream
    .map(r => (r.id, r.temperature))
    .keyBy(_._1)
    .timeWindow(Time.seconds(15))
    .reduce((r1, r2) => (r1._1, r1._2.min(r2._2)))
```

- 窗口分配器（window assigner）
  - window() 方法接收的输入参数是一个 WindowAssigner
  - WindowAssigner 负责将每条输入的数据分发到正确的 window 中
  - Flink 提供了通用的 WindowAssigner
    - 滚动窗口（tumbling window）
    - 滑动窗口（sliding window）
    - 会话窗口（session window）
    - 全局窗口（global window）

### 6.2.2 创建不同类型的窗口

- 滚动时间窗口（tumbling time window）

  **.timeWindow(Time.seconds(15))**

- 滑动事件窗口（sliding time window）

  **.timeWindow(Time.seconds(15), Time.seconds(5))**

- 会话窗口（session window）

  **.window(EventTimeSessionWindows.withGap(Time.minutes(10)))**

- 滚动计数窗口（tumbling count Window）

  **.countWindow(5)**

- 滑动计数窗口（sliding count window）

  **.countWindow(10,2)**



### 6.2.3 窗口函数（window function）

- window function 定义了要对窗口中收集的数据做的计算操作
- 可以分为两类
  - 增量聚合函数（incremental aggregation functions）
    - 每条数据到来就进行计算，保持一个简单的状态
    - ReduceFunction, AggregateFunction
- 全窗口函数（full window functions）
  - 先把窗口所有数据收集起来，等到计算的时候会遍历所有数据
  - ProcessWindowFunction

### 6.2.4  其它可选 API

- .trigger()——触发器
  - 定义了 window 什么时候关闭，触发计算并输出结果
- .evitor——移除器
  - 定义了移除某些数据的逻辑
- .allowedLateness()——允许处理迟到的数据
- .sideOutputLateData()——将迟到的数据放入侧输出流
- .getSideOutput()——获取侧输出流

![image-20200731151303049](images/getSideOutput.png)

# 第 7 章 时间语义与 Wartermark



## 7.1 Flink 中的时间语义

在 Flink 的流式处理中，会涉及到时间的不同概念，如下图所示

![image-20200731151350454](images/time-1597711596746.png)

- Event Time: 事件创建的时间
- Ingestion Time: 数据进入 Flink 的时间
- Processing Time: 执行操作算子的本地系统时间，与机器无关

**哪种时间语义更重要**

![image-20200731151731709](images/star.png)

- 不同的时间语义有不同的应用场合
- 我们往往更关心事件时间（Event Time）

![image-20200731152034421](images/EventTime.png)

- 某些应用场合，不应该使用 Processing Time

- Event Time 可以从日志数据的时间戳（timestamp）中提取

  ```txt
  2020-07-31 14:25:10.624 Fail over to rm
  ```

## 7.2 设置 EventTime

- 我们可以直接在代码中，对执行环境调用 setStreamTimeCharacteristic 方法，设置流的时间特性
- 具体的事件，还需要从数据中提取时间戳

```scala
val env = StreamExecutionEnvironment.getExecutionEnvironment

//从调用时刻开始给 env 创建的每一个 stream 追加时间特征
env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
```

## 7.3  水位线 Watermark

**乱序数据的影响**

​	    流处理从事件产生，到流经 source，再到 operator，中间是有一个过程和时间的，虽然大部分情况下，流到 operator 的数据都是按照事件产生的时间顺序来的但是也不排除由于网络、分布式等原因，导致乱序的产生，所谓乱序，就是指 Flink 接收到的事件的先后顺序不是严格按照事件的 Event Time 顺序排列的。

![image-20200731152544715](images/Watermark.png)

- 当 Flink 以 Event Time 模式处理数据流时，他会根据数据里的时间戳来处理基于事件的算子。
- 由于网络、分布式、等原因，会导致乱序数据的产生。
- 乱序数据会让窗口计算不准确。

​       那么此时出现一个问题，一旦出现乱序，如果只根据 eventTime 决定 window 的运行，我们不能明确数据是否全部到位，但又不能无限期的等下去，此时必须要有个机制来保证一个特定的时间后，必须触发 window 去进行计算了，这个特别的机制，就是 Watermark。

- Watermark 是一种衡量 Event Time 进展的机制。

- ==Watermark  是用于处理乱序事件的，而正确的处理乱序事件，通常用 Watermark 机制结合 window 来实现。==

- 数据流中的 Watermark 用于表示 timestamp 小于 Watermark 的数据，都已经到达了，因此，window 的执行也是由 Watermark 触发的。

- Watermark 可以理解成一个延迟触发机制，我们可以设置 Watermark 的延时时长 t，每次系统会校验已经到达的数据中最大的 maxEventTime，然后认定 eventTime 小于 maxEventTime - t 的所有数据都已经到达，如果有窗口的停止时间等于 maxEventTime – t，那么这个窗口被触发执行。

### 7.3.1  基本概念

- 怎样避免乱序数据带来计算不正确？
- 遇到了一个时间戳达到了窗口关闭时间按，不应该立刻触发窗口计算，二是等待一段时间，等迟到的数据来了再关闭窗口
- Watermark 是一种横拉 Event Time 进展的机制，可以设定延迟出发
- Watermark 是用来处理乱序事件的，而正确的处理乱序时间，通常用 Watermark 机制结合 window 来实现
- 数据流中的  Watermark 用于表示 timestamp 小于  Watermark 的数据，都已经到达了，因此， window 的执行也是由  Watermark 触发的。
-  Watermark 用来让程序自己平衡延迟和结果正确性

> 有序流的 Watermarker 如下图所示：（Watermark 设置为 0）

![image-20200731154221243](images/Watermark0.png)

> 乱序流的 Watermarker 如下图所示：（Watermark 设置为 2）

![image-20200731154244354](images/Watermark2.png)

​		当 Flink 接收到数据时，会按照一定的规则去生成 Watermark，这条 Watermark 就等于当前所有到达数据中的 maxEventTime - 延迟时长，也就是说，Watermark 是由数据携带的，一旦数据携带的 Watermark 比当前未触发的窗口的停止时间要晚，那么就会触发相应窗口的执行。由于 Watermark 是由数据携带的，因此，如果运行过程中无法获取新的数据，那么没有被触发的窗口将永远都不被触发。

​		上图中，我们设置的允许最大延迟到达时间为 2s，所以时间戳为 7s 的事件对应的 Watermark 是 5s，时间戳为 12s 的事件的 Watermark 是 10s，如果我们的窗口是 1s~5s，窗口 2 是 6s~10s，那么时间戳为 7s 的事件到达时的 Watermarker 恰好触发窗口 1，时间戳为 12s 的事件到达时的 Watermark 恰好触发窗口 2。

​		Watermark 就是触发前一窗口的“关窗时间”，一旦触发关门那么以当前时刻为准在窗口范围内的所有所有数据都会收入窗中。只要没有达到水位那么不管现实中的时间推进了多久都不会触发关窗。

> Watermark 的特点

![image-20200731153752595](images/Watermark5.png)

- Watermark 是一条特殊的数据记录
- Watermark 必须单调递增，以确保任务的时间按事件时间时钟在向前推进，而不是后退
- Watermark 与数据的时间戳相关

### 7.3.2 Watermark  的传递、引入和设定

> Watermark 的传递

![image-20200731154018543](images/Watermarkcd.png)

> Watermark 的引入

- Event Time 的使用一定要指定数据源中的时间戳
- 对于排好序的数据,只需要指定时间戳就够了, 不需要延迟触发,可以指定时间戳就行了

```scala
//对于乱序数据，最常见的引用方式
dataStream.assignTimestampsAndWatermarks( new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.milliseconds(1000)) {
    override def extractTimestamp(element: SensorReading): Long = {
    	element.timestamp * 1000
    }
})
```

- 对于排好序的数据,不需要延迟触发,可以只指定时间戳就行了

```scala
//注意单位是毫秒,所有根据时间戳的不同,可能需要再乘1000
dataStream.assignAscendingTimestamps(_.timestamp * 1000)
```

- Flink 暴露了 TimestampAssigner 接口供我们实现,使我们可以自定义如何从事件数据中抽取时间戳和生成 watermark

  ```scala
  dataStream.assignTimestampsAndWatermarks(new MyAssigner())
  ```

  - MyAssigner 可以有两种类型,都继承自 TimestampAssigner

- Event Time 的使用一定要指定数据源中的时间戳
- 调用 assigntimestampAndWatermarks 方法,传入一个 BoundedOutOfOrdernessTimestampExtracter,就可以指定 watermark

```scala
dataStream.assignTimestampsAndWatermarks( 
new BoundedOutOfOrdernessTimestampExtractor[SensorReading]
	(Time.milliseconds(1000)) {
    	override def extractTimestamp(element: SensorReading): Long = {
    		element.timestamp * 1000
    }
})
```

> TimestampAssigner

- 定义了抽取时间戳,以及生成 watermark 的方法,有两种类型

  - AssignerWithPeriodicWatermarks

    - 周期性的生成 watermark, 系统会周期性的将 watermark 插入到流中(水位线也是一种特殊的事件!)。默认周期是 200 毫秒。可以使用 ExecutionConfig.setAutoWatermarklnterval() 方法周期性watermark 的.

    ```scala
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    
    //每隔 5 秒产生一个 watermark
    env.getConfig.setAutoWatermarkInterval(5000)
    ```

    > 产生 watermark 的逻辑：每隔 5 秒钟，Flink 会调用 AssignerWithPeriodicWatermarks 的 getCurrentWatermark()方法。如果方法返回一个时间戳大于之前水位的时间戳，新的 watermark 会被插入到流中。这个检查保证了水位线是单调递增的。如果方法返回的时间戳小于等于之前水位的时间戳，则不会产生新的 watermark。

    ```scala
    //自定义一个周期性的时间戳抽取
    class PeriodicAssigner extends
    AssignerWithPeriodicWatermarks[SensorReading] {
        val bound: Long = 60 * 1000 //延时为 1 分钟
        var maxTs: Long = Long.MinValue //观察到的最大时间戳
        override def getCurrentWatermark: Watermark = {
        new Watermark(maxTs - bound)
        }
        override def extractTimestamp(r: SensorReading, previousTS: Long) = {
            maxTs = maxTs.max(r.timestamp)
            r.timestamp
        }
    }
    ```

    > 一种简单的特殊情况是，如果我们事先得知数据流的时间戳是单调递增的，也就是说没有乱序，那我们可以使用 assignAscendingTimestamps，这个方法会直接使用数据的时间戳生成 watermark。

    ```scala
    val stream: DataStream[SensorReading] = ...
    val withTimestampsAndWatermarks = stream
    	.assignAscendingTimestamps(e => e.timestamp)
    >> result: E(1), W(1), E(2), W(2), ...
    ```

    > 而对于乱序数据流，如果我们能大致估算出数据流中的事件的最大延迟时间，
    > 就可以使用如下代码

    ```scala
    val stream: DataStream[SensorReading] = ...
    val withTimestampsAndWatermarks = stream.assignTimestampsAndWatermarks(
    	new SensorTimeAssigner
    )
    
    class SensorTimeAssigner extends
    BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.seconds(5)) {
    	//抽取时间戳
    	override def extractTimestamp(r: SensorReading): Long = r.timestamp
    }
    
    >> relust: E(10), W(0), E(8), E(7), E(11), W(1), ...
    ```

    

    - 升序和前面乱序的处理 BoundedOutOfOrderness, 都是基于周期性 watermark 的.

  - AssignerWithPunctuatedWatermarks

    - 没有时间周期规律,可大段的生成 watermark

    > 间断式地生成 watermark。和周期性生成的方式不同，这种方式不是固定时间的，而是可以根据需要对每条数据进行筛选和处理。直接上代码来举个例子，我们只给 sensor_1 的传感器的数据流插入 watermark

    ```scala
    class PunctuatedAssigner extends AssignerWithPunctuatedWatermarks[SensorReading] {
    	val bound: Long = 60 * 1000
    	override def checkAndGetNextWatermark(r: SensorReading, extractedTS: Long): Watermark = {
        if (r.id == "sensor_1") {
               new Watermark(extractedTS - bound)
            } else {
                null
            }
        }
        override def extractTimestamp(r: SensorReading, previousTS: Long): Long = {
        	r.timestamp
        }
    }
    ```

> Watermark 的设定

- 在 Flink 中, watermark 由应用程序开发人员生成,这通常需要对相应领域有一定的理解
- 如果 watermark 设置的延迟太久,收到结果的速度可能就会很慢,解决办法是在水位线到达之前输出一个近似结果
- 而如果 watermark 到达得太早,则可能收到错误结果,不过 Flink 处理迟到数据的机制可以解决这个问题

## 7.4 EvnetTime 在 window 中的使用

### 7.4.1  滚动窗口（TumblingEventTimeWindows ）

```scala
def main(args: Array[String]): Unit = {
// 环境
	val env: StreamExecutionEnvironment =
	StreamExecutionEnvironment.getExecutionEnvironment
	env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
	env.setParallelism(1)
	val dstream: DataStream[String] = env.socketTextStream("localhost",7777)
	val textWithTsDstream: DataStream[(String, Long, Int)] = dstream.map
	{ text =>
    	val arr: Array[String] = text.split(" ")
    	(arr(0), arr(1).toLong, 1)
    }
    
    val textWithEventTimeDstream: DataStream[(String, Long, Int)] =
     textWithTsDstream.assignTimestampsAndWatermarks(new
     BoundedOutOfOrdernessTimestampExtractor[(String, Long, Int)](Time.milliseconds(1000)) {
   	 	override def extractTimestamp(element: (String, Long, Int)): Long = {
    	return element._2
      }
    })
    val textKeyStream: KeyedStream[(String, Long, Int), Tuple] = textWithEventTimeDstream.keyBy(0)
    textKeyStream.print("textkey:")
    val windowStream: WindowedStream[(String, Long, Int), Tuple, TimeWindow]
    = textKeyStream.window(TumblingEventTimeWindows.of(Time.seconds(2)))
    
    val groupDstream: DataStream[mutable.HashSet[Long]] =
    windowStream.fold(new mutable.HashSet[Long]()) { 
        case (set, (key, ts, count))  =>  set += ts
    }
    
    groupDstream.print("window::::").setParallelism(1)
    env.execute()
    }
}
```

结果是按照 Event Time 的时间窗口计算得出的，而无关系统的时间（包括输入的快慢）

### 7.4.2  滑动窗口（SlidingEventTimeWindows

```scala
def main(args: Array[String]): Unit = {

	// 环境
	val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

	env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
	env.setParallelism(1)
	val dstream: DataStream[String] = env.socketTextStream("localhost",7777)

	val textWithTsDstream: DataStream[(String, Long, Int)] = dstream.map {text => val arr: Array[String] = text.split(" ")
		(arr(0), arr(1).toLong, 1)
	}

	val textWithEventTimeDstream: DataStream[(String, Long, Int)] = textWithTsDstream
		.assignTimestampsAndWatermarks(new 
			BoundedOutOfOrdernessTimestampExtractor[(String, Long, Int)](Time.milliseconds(1000)){
				override def extractTimestamp(element: (String, Long, Int)): Long = {
				return element._2
			}
	})

	val textKeyStream: KeyedStream[(String, Long, Int), Tuple] = textWithEventTimeDstream.keyBy(0)

	textKeyStream.print("textkey:")

	val windowStream: WindowedStream[(String, Long, Int), Tuple, TimeWindow] =
	textKeyStream.window(SlidingEventTimeWindows.of(Time.seconds(2),Time.millis
		
	econds(500)))

	val groupDstream: DataStream[mutable.HashSet[Long]] = windowStream.fold(new
	mutable.HashSet[Long]()) { case (set, (key, ts, count)) => set += ts
	}

	groupDstream.print("window::::").setParallelism(1)
	env.execute()
}
```

### 7.4.3  会话窗口（EventTimeSessionWindows ）

相邻两次数据的 EventTime 的时间差超过指定的时间间隔就会触发执行。如果加入 Watermark， 会在符合窗口触发的情况下进行延迟。到达延迟水位再进行窗口触发。

```scala
def main(args: Array[String]): Unit = {
	// 环境
	val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
	env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
	env.setParallelism(1)

	val dstream: DataStream[String] = env.socketTextStream("localhost",7777)
	val textWithTsDstream: DataStream[(String, Long, Int)] = dstream.map {text =>
		val arr: Array[String] = text.split(" ")
		(arr(0), arr(1).toLong, 1)
	}
		val textWithEventTimeDstream: DataStream[(String, Long, Int)] =
		textWithTsDstream.assignTimestampsAndWatermarks(new
		BoundedOutOfOrdernessTimestampExtractor[(String, Long,
		Int)](Time.milliseconds(1000)) {
		override def extractTimestamp(element: (String, Long, Int)): Long = {
		return element._2
		}
	})
	val textKeyStream: KeyedStream[(String, Long, Int), Tuple] = textWithEventTimeDstream.keyBy(0)

	textKeyStream.print("textkey:")

	val windowStream: WindowedStream[(String, Long, Int), Tuple, TimeWindow] = textKeyStream
		.window(EventTimeSessionWindows.withGap(Time.milliseconds(500)))

	windowStream.reduce((text1,text2)=>(text1._1,0L,text1._3+text2._3))
		.map(_._3)
		.print("windows:::")
		.setParallelism(1)
	env.execute()
}
```

# 第 8 章 ProcessFunction API（底层 API）

​      我们之前学习的 转换算子是无法访问事件的时间戳信息和水位线信息的。而这在一些应用场景下，极为重要。例如 MapFunction 这样的 map 转换算子就无法访问时间戳或者当前事件的事件时间。

​		基于此，DataStream API 提供了一系列的 Low-Level 转换算子。可以 访问时间戳、watermark  以及注册定时事件。还可以输出 特定的一些事件，例如超时事件等。==Process Function 用来构建事件驱动的应用以及实现自定义的业务逻辑(使用之前的window 函数和转换算子无法实现)==。例如，Flink SQL 就是使用 Process Function 实现的。

Flink 提供了 8 个 Process Function：

- ProcessFunction
- KeyedProcessFunction
- CoProcessFunction
- ProcessJoinFunction
-  BroadcastProcessFunction
- KeyedBroadcastProcessFunction
- ProcessWindowFunction
- ProcessAllWindowFunction

## 8.1 KeyedProcessFunction

这里我们重点介绍 KeyedProcessFunction。

​		==KeyedProcessFunction 用来操作 KeyedStream==。KeyedProcessFunction 会==处理流的每一个元素，输出为 0 个、1 个或者多个元素==。所有的 Process Function 都继承自 RichFunction 接口，所以都有 open()、close()和 getRuntimeContext()等方法。而 KeyedProcessFunction[KEY, IN, OUT] 还额外提供了两个方法

- processElement(v: IN, ctx: Context, out: Collector[OUT]), 流中的每一个元素都会调用这个方法，调用结果将会放在 Collector 数据类型中输出。Context 可以访问元素的时间戳，元素的 key，以及 TimerService 时间服务。Context 还可以将结果输出到别的流(side outputs)。
- onTimer(timestamp: Long, ctx: OnTimerContext, out: Collector[OUT]) 是一个回调函数。当之前注册的定时器触发时调用。参数 timestamp 为定时器所设定的触发的时间戳。Collector 为输出结果的集合。OnTimerContext 和 processElement 的 Context 参数一样，提供了上下文的一些信息，例如定时器触发的时间信息(事件时间或者处理时间)。

## 8.2 TimerService 和 定时器（Timers）

Context 和 OnTimerContext 所持有的 TimerService 对象拥有以下方法

- currentProcessingTime(): Long 返回当前处理时间
- currentWatermark(): Long 返回当前 watermark 的时间戳
- registerProcessingTimeTimer(timestamp: Long): Unit 会注册当前 key 的 processing time 的定时器。当 processing time 到达定时时间时，触发 timer。
- registerEventTimeTimer(timestamp: Long): Unit 会注册当前 key 的 event time 定时器。当水位线大于等于定时器注册的时间时，触发定时器执行回调函数。
- deleteProcessingTimeTimer(timestamp: Long): Unit 删除之前注册处理时间定时器。如果没有这个时间戳的定时器，则不执行。
- deleteEventTimeTimer(timestamp: Long): Unit 删除之前注册的事件时间定时器，如果没有此时间戳的定时器，则不执行。

​    当定时器 timer 触发时，会执行回调函数 onTimer()。注意定时器 timer 只能在 keyed streams 上面使用。

> 下面举个例子说明 KeyedProcessFunction 如何操作 KeyedStream。

> 需求：监控温度传感器的温度值，如果温度值在一秒钟之内(processing time)连
> 续上升，则报警。

```scala
val warnings = readings
    .keyBy(_.id)
    .process(new TempIncreaseAlertFunction)
```

> 看一下 TempIncreaseAlertFunction 如何实现, 程序中使用了 ValueState 这样一个状态变量。

```scala
class TempIncreaseAlertFunction extends KeyedProcessFunction[String, SensorReading, String] {

    //保存上一个传感器温度值
    lazy val lastTemp: ValueState[Double] = getRuntimeContext.getState(
    	new ValueStateDescriptor[Double]("lastTemp", Types.of[Double])
    )
    
    //保存注册的定时器的时间戳
    lazy val currentTimer: ValueState[Long] = getRuntimeContext.getState(
    	new ValueStateDescriptor[Long]("timer", Types.of[Long])
    )
    
    override def processElement(r: SensorReading,
    ctx: KeyedProcessFunction[String, SensorReading, String]#Context,
    out: Collector[String]): Unit = {
        
        //取出上一次的温度
        val prevTemp = lastTemp.value()
        
        //将当前温度更新到上一次的温度这个变量中
        lastTemp.update(r.temperature)
        val curTimerTimestamp = currentTimer.value()
        if (prevTemp == 0.0 || r.temperature < prevTemp) {
            
            //温度下降或者是第一个温度值，删除定时器
            ctx.timerService().deleteProcessingTimeTimer(curTimerTimestamp)
            
            //清空状态变量
            currentTimer.clear()
        } else if (r.temperature > prevTemp && curTimerTimestamp == 0) {
            
            //温度上升且我们并没有设置定时器
            val timerTs = ctx.timerService().currentProcessingTime() + 1000
            ctx.timerService().registerProcessingTimeTimer(timerTs)
            currentTimer.update(timerTs)
        }
    }
    override def onTimer(ts: Long,
    ctx: KeyedProcessFunction[String, SensorReading, String]#OnTimerContext,
    out: Collector[String]): Unit = {
        out.collect("传感器 id 为: " + ctx.getCurrentKey + "的传感器温度值已经连续 1s 上升了。")
        currentTimer.clear()
    }
}
```

## 8.3 侧输出流（SideOutput）

​        大部分的 DataStream API 的算子的输出是单一输出，也就是某种数据类型的流。除了 split 算子，可以将一条流分成多条流，这些流的数据类型也都相同。processfunction 的 side outputs 功能可以产生多条流，并且这些流的数据类型可以不一样。一个 side output 可以定义为 OutputTag[X]对象，X 是输出流的数据类型。processfunction 可以通过 Context 对象发射一个事件到一个或者多个 sideoutputs。

> 下面是一个示例程序

```scala
val monitoredReadings: DataStream[SensorReading] = readings
 .process(new FreezingMonitor)monitoredReadings
 .getSideOutput(new OutputTag[String]("freezing-alarms"))
 .print()
readings.print()
```

> 接下来我们实现 FreezingMonitor 函数，用来监控传感器温度值，将温度值低于 32F 的温度输出到 side output。

```scala
class FreezingMonitor extends ProcessFunction[SensorReading, SensorReading] {
    
    //定义一个侧输出标签
    lazy val freezingAlarmOutput: OutputTag[String] = new OutputTag[String]("freezing-alarms")
    
    override def processElement(r: SensorReading,
    ctx: ProcessFunction[SensorReading, SensorReading]#Context,
    out: Collector[SensorReading]): Unit = {
        
    	//温度在 32F 以下时，输出警告信息
    	if (r.temperature < 32.0) {
    		ctx.output(freezingAlarmOutput, s"Freezing Alarm for ${r.id}")
         }
    //所有数据直接常规输出到主流
    out.collect(r)
    }
}
```

## 8.4 CoProcessFunction

​		对于两条输入流，DataStream API 提供了 CoProcessFunction 这样的 low-level 操作。

CoProcessFunction 提供了操作每一个输入流的方法: processElement1() 和 processElement2()。类似

于 ProcessFunction，这两种方法都通过 Context 对象来调用。这个 Context 对象可以访问事件数据,定

时器时间戳，TimerService，以及 side outputs。CoProcessFunction 也提供了 onTimer()回调函数。

# 第 9 章 状态编程和容错机制

## 9.1 有状态的算子和应用程序

​		流式计算分为无状态和有状态两种情况。无状态的计算观察每个独立事件，并根据最后一个事件输出结果。例如，流处理应用程序从传感器接收温度读数，并在温度超过 90 度时发出警告。有状态的计算则会基于多个事件输出结果。

​	    无状态流处理分别接收每条数据记录(图中的黑条)，然后根据最新输入的数据生成输出数据(白条)。	    有状态流处理会维护状态(根据每条输入记录进行更新)，并基于最新输入的记录和当前的状态值生成输出记录(灰条)。

![image-20200731181054194](images/statesuanzi.png)

​		上图中输入数据由黑条表示。无状态流处理每次只转换一条输入记录，并且仅根据最新的输入记录输出结果(白条)。有状态流处理维护所有已处理记录的状态值，并根据每条新输入的记录更新状态，因此==输出记录(灰条)反映的是综合考虑多个事件之后的结果==。

​		尽管无状态的计算很重要，但是流处理对有状态的计算更感兴趣。事实上，正确地实现有状态的计

算比实现无状态的计算难得多。旧的流处理系统并不支持有状态的计算，而新一代的流处理系统则将状

态及其正确性视为重中之重。

>  Flink 中的状态

![image-20200731180657784](images/Flinkstate.png)

- 由一个任务维护，并且用来计算某个结果的所有数据，都属于这个任务的状态
- 可以认为任务状态就是一个本地变量，可以被任务的业务逻辑访问
- Flink 会进行状态管理，包括状态一致性、故障处理以及高效存储和访问，以便开发人员可以专注于应用程序的逻辑
- 在 Flink 中，状态始终与特定算子相关联
- 为了使用时的 Flink 了解算子的状态，算子需要预先注册其状态

总的来说，有两种状态

- 算子状态（Operator State）
  - 算子状态的作用范围限定为算子任务
- 键控状态（Keyed State）
  - 根据输入数据流中定义的键（key）来维护和访问

### 9.3.1  算子状态（operator state ）

![image-20200731181914534](images/operatorstate.png)

- 算子状态的作用范围限定为算子任务。这意味着由同一并行任务所处理的所有
  数据都可以访问到相同的状态
- 状态对于同一任务而言是共享的。
- 算子状态不能由相同或不同算子的另一个任务访问。

**算子状态数据结构**

- 列表状态（List state）
  - 将状态表示为一组数据的列表。
- 联合列表状态
  - 将状态表示为数据的列表。它与常规列表状态的区别在于，在发生故障时，或者从保存点（savepoint）启动应用程序时如何恢复。
- 广播状态（Broadcast state）
  - 如果一个算子有多项任务，而它的每项任务状态又都相同，那么这种特殊情况最适合应用广播状态。

### 9.3.2  键控状态（keyed state ）

- 键控状态是根据输入数据流中定义的键（key）来维护和访问的。
- Flink 为每个键值维护一个状态实例，并将具有相同键的所有数据，都分区到同一个算子任务中，这个任务会维护和处理这个 key 对应的状态。
- 当任务处理一条数据时，它会自动将状态的访问范围限定为当前数据的 key。因此，具有相同 key 的所有数据都会访问相同的状态。Keyed State 很类似于一个分布式的 key-value map 数据结构，只能用于 KeyedStream（keyBy 算子处理之后）。

![image-20200731182546846](images/keyedstate.png)

 **键控状态数据结构 **

- 值状态（Value state）
  - 将状态表示为单个的值
- 列表状态（List state）
  - 将状态表示为一组数据的列表
- 映射状态（Map state）
  - 将状态表示为一组 Key-Value 对
- 聚合状态（Reducing state & Aggregating State）
  - 将状态表示为一个用于聚合操作的列表

 **键控状态的使用 **

- 声明一个键控状态

  ```scala
  lazy val lastTemp: ValueState[Double] = getRuntimeContext.getState[Double]{
  	new ValueStateDesciptor[Double]("lastTemp", classof[Double])
  }
  ```

- 读取状态

  ```scala
  val prevTemp = lastTemp.value()
  ```

- 对状态赋值

  ```scala
  lastTemp.update( value.temperatur )
  ```

**状态后端（State Backends）**

- 每传入一条数据，有状态的算子任务都会读取和更新状态
- 由于有效的状态访问对于处理数据的低延迟至关重要，因此每个并行任务都会在本地维护其状态，以确保快速的状态访问
- 状态的存储、访问以及维护，由一个可插入的组件决定，这个组件就叫做状态后端（state backend）
- 状态后端主要负责两件事：本地的状态管理，以及检查点（checkpoint）状态写入远程存储

## 9.2 状态一致性

> 什么是状态一致性

![image-20200731195617016](images/sateyzx.png)

- 有状态的流处理，内部每个算子任务都可以有自己的状态。
- 对于流处理器内部，所谓的状态一致性，其实就是我们所说的计算结果要保证准确。
- 一条数据不应该丢失，也不应该重复计算。
- 在遇到故障时可以恢复状态，恢复以后的重新计算，结果应该也是完全正确的。

### 9.2.1 一致性级别

- AT-MOST-ONCE（最多一次）
  - 当任务故障时，最简单的做法是什么都不敢，既不恢复丢失的状态，也不传播丢失的数据。At-most-once语义的含义是最多处理一次事件
- AT-LAST-ONCE（至少一次）
  - 在大多数的真实应用场景，我们希望不丢失事件。这种类型的保障称为 at-least-once，意思是所有的事件都得到了处理，而一些事件还可以被处理多次。
- EXACTLY-ONCE（精确一次）
  - 恰好处理一次是最严格的保证，也是最难实现的。恰好处理一次语义不仅仅意味着没有事件丢失，还意味者针对每一个数据，内部状态仅仅更新一次。

### 9.2.2  端到端（end-to-end ）状态一致性

- 目前我们看到的一致性保证都是由流处理器实现的，也就是说都是在 Flink 流处理器内部保证的；而在真实应用中，流处理应用除了流处理器以外还包含了数据源（例如 Kafka）和输出到持久化系统
- 端到端的一致性保障，意味着结果的正确性贯穿了整个流处理应用的始终；每一个组件都保证了它自己的一致性
- 整个端到端的一致性级别取决于所有组件中一致性最弱的组件

**端到端 end-to-end** 

- 内部保证—— checkpoint

- source 端——可重设数据的读取位置

- sink 端——从故障恢复时，数据不会重复写入外部系统

  - 幂等写入

    - 所谓==幂等操作，是说一个操作，可以重复执行很多次，但只导致一次结果更改，也就是说，后面再重复执行就不起作用了==。

    ![image-20200731202354104](images/sinkpairs.png)

  - 事务写入

    - 事务（Transaction）
      - 应用程序中一系列严密的操作，所有操作必须成功完成，否则在每个操作中所做的所有更改都会被撤销。
      - 具有原子性：一个事务中的一系列的操作要么全部成功，要么一个都不做。
    - 实现思想：构建的事务对应着 checkpoint，等到 checkpoint 真正完成的时候，才能把所有对应的结果写入 sink 系统中。
    - 实现方式
      - 预写日志（Write-Ahead-log, WAL）
        - 把结果数据先写成状态保存，然后再受到 checkpoint 完成的通知时，一次性写入 sink 系统。
        - 简单易于实现，由于数据提前在状态后端中做了缓存，所以无论 sink 系统，都能用这种方式一批搞定。
        - DataStream API 提供了一个模板类：GenericWriteAheadSink,来实现这种事务性sink。
      - 两阶段提交
        - 对于每个 checkpoin, sink 任务会启动一个事务，并将接下来所有接收的数据添加到事务里。
        - 然后将这些数据写入外部 sink 系统，但不提交他们——这时只是“预提交“。
        - 当他收到 checkpoint 完成的通知时，他才会正式提交事务，实现结果的真正写入。
        - 这种方式真正实现了 exactly-once，它需要一个提供事务支持的外部 sink 系统。Flink 提供了 TwoPhaseCommitSinkFuntion 接口。

**2PC(两阶段提交) 对外部 sink 系统的要求**

- 外部 sink 系统必须提供事务支持，或者 sink 任务必须能够模拟外部系统上的任务。
- 在 checkpoint的间隔期间里，必须能够开启一个事务并接受数据写入。
- 在收到 checkpoin 完成的通知之前，事务必须时”等待提交“ 的状态。在故障恢复的情况下，这可能需要一些时间。如果这个时候 sink 系统关闭事务（例如超时了），那么未提交的数据就会丢失。
- sink 任务必须能够在进程失败后恢复事务。
- 提交事务必须是幂等操作。

**不同 source 和 Sink 的一致性保证**

![image-20200731204109491](images/image-20200731204109491.png)

## 9.3 检查点（checkpoint）

​		Flink 具体如何保证 exactly-once 呢? 它使用一种被称为"检查点"（checkpoint）的特性，在出现故障时将系统重置回正确状态。下面通过简单的类比来解释检查点的作用。

​		假设你和两位朋友正在数项链上有多少颗珠子，如下图所示。你捏住珠子，边数边拨，每拨过一颗珠子就给总数加一。你的朋友也这样数他们手中的珠子。当你分神忘记数到哪里时，怎么办呢? 如果项链上有很多珠子，你显然不想从头再数一遍，尤其是当三人的速度不一样却又试图合作的时候，更是如此(比如想记录前一分钟三人一共数了多少颗珠子，回想一下一分钟滚动窗口)。

![image-20200731193750717](images/image-20200731193750717.png)

​		于是，你想了一个更好的办法: 在项链上每隔一段就松松地系上一根有色皮筋，将珠子分隔开; 当珠子被拨动的时候，皮筋也可以被拨动; 然后你安排一个助手，让他在你和朋友拨到皮筋时记录总数。用这种方法，当有人数错时，就不必从头开始数。相反，你向其他人发出错误警示，然后你们都从上一根皮筋处开始重数，助手则会告诉每个人重数时的起始数值，例如在粉色皮筋处的数值是多少。

​		Flink 检查点的作用就类似于皮筋标记。数珠子这个类比的关键点是: 对于指定的皮筋而言，珠子的相对位置是确定的; 这让皮筋成为重新计数的参考点。总状态(珠子的总数)在每颗珠子被拨动之后更新一次，助手则会保存与每根皮筋对应的检查点状态，如当遇到粉色皮筋时一共数了多少珠子，当遇到橙色皮筋时又是多少。当问题出现时，这种方法使得重新计数变得简单。

> 一致性检查点（Checkpoints）

- Flink 使用了一种轻量级别快照机制 —— 检查点（checkpoint）来保证 exactly-once 语义
- 有状态流应用的一致性检查点，其实就是：所有任务的状态，在某个时间点的一份拷贝（一份快照）。而这个时间点，应该是所有任务都恰好处理完了一个相同的输入数据的时候。
- 应用状态的一致检查点，是 Flink 故障机制的核心

![image-20200731190119546](images/Checkpoints.png)

- Flink 故障恢复机制的核心，就是应用状态的一致性检查点
- 有状态流应用的一致性检查点，其实就是所有任务的状态，在某个时间点的一份拷贝（一份快照）；这个时间点，应该是所有任务都恰好处理万一个相同的输入数据的时候

> 从检查点到恢复状态

![image-20200731190447730](images/Checkpoints7.png)

- 在执行流应用程序期间，Fink 会定期保存状态的一致性检查点
- 如果发生故障，Flink 将会使用最近的检查点来一致回复应用程序的状态，并重新启动处理流程

![image-20200731190805308](images/Checkpoints8.png)

- 遇到故障之后，第一步就是重启应用

![image-20200731190901353](images/image-20200731190901353.png)

- 第二步就是从 checkpoint 中读取状态，将状态重置
- 从检查点重新启动应用程序后，其内部状态与检查点完成时的状态完全相同

![image-20200731191047010](images/Checkpoints10.png)

- 第三步：开始消费并处理检查点到发生故障之间的所有数据
- 这种检查点的保存和恢复机制可以为应用程序状态提供 “精确一次”（exactly-once）的一致性，因为所有算子都会保存检查点并恢复其所有状态，这样一来所有的输入流就都会被重置到检查点完成时的位置。

> 检查点的实现算法

- 一种简单的想法
  - 暂停应用，保存状态到检查点，再重新恢复应用
- Flink 的改进实现
  - 基于 Chandy-Lamport 算法的分布式快照。
  - 将检查点的保存和数据处理分离开，不暂停整个应用。

### 9.3.1 Flink 的检查点算法

​        Flink 检查点的核心作用是确保状态正确，即使遇到程序中断，也要正确。记住这一基本点之后，我们用一个例子来看检查点是如何运行的。Flink 为用户提供了用来定义状态的工具。例如，以下这个 Scala 程序按照输入记录的第一个字段(一个字符串)进行分组并维护第二个字段的计数状态。

```scala
val stream: DataStream[(String, Int)] = ...
val counts: DataStream[(String, Int)] = stream
  .keyBy(record => record._1)
  .mapWithState( (in: (String, Int), state: Option[Int]) =>
    state match {
      case Some(c) => ( (in._1, c + in._2), Some(c + in._2) )
      case None => ( (in._1, in._2), Some(in._2) )
})
```

​       该程序有两个算子: keyBy 算子用来将记录按照第一个元素(一个字符串)进行分组，根据该 key 将数据进行重新分区，然后将记录再发送给下一个算子: 有状态的 map 算子(mapWithState)。map 算子在接收到每个元素后，将输入记录的第二个字段的数据加到现有总数中，再将更新过的元素发射出去。下图表示程序的初始状态: 输入流中的 6 条记录被检查点分割线(checkpoint barrier)隔开，所有的 map 算子状态均为 0(计数还未开始)。所有 key 为 a 的记录将被顶层的 map 算子处理，所有 key 为 b 的记录将被中间层的 map 算子处理，所有 key 为 c 的记录则将被底层的 map 算子处理。

- 检查点分界线（Checkpoint Barrier）
  - Flink 的检查点算法用到了一种称为分界线（barrier）的特殊数据形式，用来把一条流 上的苏韩剧按照不同的检查点分开
  - 分界线之前到来的数据导致的状态更改，都会被包含在当前分界线所属的检查点中，而基于分界线之后的数据导致的所有更改，就会被包含在之后的检查点中

![image-20200731191948231](images/checkpointbarrier.png)

- 现在是一个有两个输入流的应用程序，用并行的两个 Source 任务来读取

![image-20200731192113987](images/image-20200731192113987.png)

- JobManager 会向每个 source 任务发送一条带有新检查点 ID 的消息，通过这种方式来启动检查点

![image-20200731192312141](images/checkpointJobmanager.png)

- 数据源将他们的状态写入检查点，并发出一个检查点 barier。
- 状态后端在状态存入检查点之后，会返回通知给 source 任务，source 任务就会向 JobManager 确认检查点完成。

![image-20200731192608690](images/Checkpointsource%20.png)

- 分界线对齐：barrier 向下游传递，sum 任务会等待所有输入分区的 battier 到达。
- 对于 barrier 已经到达的分区，继续到达的数据会被缓存。
- 而 barrier 尚未到达的分区，数据会被正常处理。

![image-20200731192841893](images/barrier.png)

- 当所有的输入分区的 barrier 时，任务就将其状态保存到状态后端的检查点中，然后 barrier 继续向下游转发。

![image-20200731193046208](images/barrierbuffer.png)

- 向下游转发检查点 barrier 后，任务继续正常的数据处理

> 保存点（Savepoints）

- Flink 还提供了可以自定义的镜像保存功能，就是保存点（savepoints）。
- 原则上，创建保存点使用的算法与检查点完全相同，因此保存点可以认为就是具有一些额外元数据的检查点。
- Flink 不会自动创建保存点，因此用户（或者外部调度程序）必须明确的触发创建操作。
- 保存点是一个强大的功能。除了故障回复外，保存点可以用于：有计划的手动备份，更新应用程序，版本迁移，暂停和重启应用等等。

### 9.3.2 Flink+Kafka  如何实现端到端的 exactly-once  语义

我们知道，端到端的状态一致性的实现，需要每一个组件都实现，对于 Flink + Kafka 的数据管道系统（Kafka 进、Kafka 出）而言，各组件怎样保证 exactly-once 语义呢？

- 内部 —— 利用 checkpoint 机制，把状态存盘，发生故障的时候可以恢复，保证内部的状态一致性
- source —— ==kafka consumer 作为 source，可以将偏移量保存下来==，如果后续任务出现了故障，恢复的时候可以由连接器重置偏移量，重新消费数据，保证一致性。
- sink —— kafka producer 作为 sink，采用两阶段提交 sink，需要实现一个TwoPhaseCommitSinkFunction

​        内部的 checkpoint 机制我们已经有了了解，那 source 和 sink 具体又是怎样运行的呢？接下来我们逐步做一个分析。我们知道 Flink 由 JobManager 协调各个 TaskManager 进行 checkpoint 存储，checkpoint 保存在StateBackend 中，默认 StateBackend 是内存级的，也可以改为文件级的进行持久化保存。

> Exactly-once 两阶段提交

- JobManager 协调各个 TaskManager 进行 checkpoint 存储
- Checkpoint 保存在 StateBackend 中，默认 StateBackend 是内存级的，也可以改为文件级的进行持久化保存

![image-20200731194410270](images/Exactly-once.png)

- 当 checkpoint 启动时，JobManager 会将检查点分界线（barrier）注入数据流；
- barrier 会在算子间传递下去。

![image-20200731194437636](images/checkpoint-inject-barrier.png)

- 每个算子会对当前的状态做个快照，保存到状态后端。

- checkpoin 机制可以保证内部的状态一致性，对于 source 任务而言，就会把当前的 offset 作为状态保存起来。下次从 checkpoint 恢复时，source 任务可以重新提交偏移量，从上次保存的位置开始重新消费数据。

![image-20200731194510661](images/Exactly-once-towphasecommit.png)

- 每个内部的 transform 任务遇到 barrier 时，都会把状态存到 checkpoint 里。

- sink 任务首先把数据写入外部 kafka，这些数据都属于预提交的事务（还不能被消费）；当遇到 barrier 时，把状态保存到状态后端，并开启新的预提交事务。

![image-20200731194543348](images/Exactly-once-towphasecommit2.png)

- 当所有算子任务的快照完成，也就是这次的 checkpoint 完成时，JobManager 会向所有任务发通知，确认这次 checkpoint 完成。

- 当 sink 任务收到确认通知，就会正式提交之前的事务，kafka 中未确认的数据就改为“已确认”，数据就真正可以被消费了。

![image-20200731194611241](images/Exactly-once-towphasecommit3.png)

​		所以我们看到，执行过程实际上是一个两段式提交，每个算子执行完成，会进行“预提交”，直到执行完 sink 操作，会发起“确认提交”，如果执行失败，预提交会放弃掉。

> Exactly-once 两阶段提交步骤

- 第一条数据来了之后，开启一个 kafka 的事务（transaction），正常写入 kafka 分区日志但标记为未提交，这就是“预提交”。

-  jobmanager 触发 checkpoint 操作，barrier 从 source 开始向下传递，遇到 barrier 的算子将状态存入状态后端，并通知 jobmanager。

-  sink 连接器收到 barrier，保存当前状态，存入 checkpoint，通知 jobmanager，并开启下一阶段的事务，用于提交下个检查点的数据。

- jobmanager 收到所有任务的通知，发出确认信息，表示 checkpoint 完成。

-  sink 任务收到 jobmanager 的确认信息，正式提交这段时间的数据。

- 外部 kafka 关闭事务，提交的数据可以正常消费了。

  所以我们也可以看到，如果宕机需要通过 StateBackend 进行恢复，只能恢复所有确认提交的操作。

## 9.4 选择一个状态后端(state backend)

- MemoryStateBackend
  - 内存级的状态后端，会将键控状态作为内存中的对象进行管理，将他们存储在 TaskManager 的 JVM 堆上，而将 checkpoin 存储在 JobManager 的内存中。
  - 特点：快速、低延迟，但不稳定。
- FsStateBacked
  - 将 checkpoint 存到远程的持久化文件系统（FileSystem）上，而对于本地状态，跟 MemoryStateBakced 一样，也会存在 TaskManager 的 JVM 堆上。
  - 同时拥有内存级的本地访问速度，和更好的容错保证。
- RocksDBStateBacked
  - 将所有状态序列化后，存入本地的 RocksDB 中存储。

```xml
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-statebackend-rocksdb_2.11</artifactId>
    <version>1.7.2</version>
</dependency>
```

> 设置状态后端为 FsStateBackend

```scala
val env = StreamExecutionEnvironment.getExecutionEnvironment
val checkpointPath: String = ???
val backend = new RocksDBStateBackend(checkpointPath)
env.setStateBackend(backend)
env.setStateBackend(new FsStateBackend("file:///tmp/checkpoints"))
env.enableCheckpointing(1000)

// 配置重启策略
env.setRestartStrategy(RestartStrategies.fixedDelayRestart(60, Time.of(10,
TimeUnit.SECONDS)))
```

# 第 10 章 Table API 与 SQL

- Flink 对批处理和流处理，提供了统一的上层 API

- Table API 可以基于流输入或者输入来运行而不需要进行任何修改

- Table API 是一套内嵌在 Java 和 Scala 语言中的查询 API，它允许以非常直观的方式组合来自一些关系运算符的查询

- Flink 的 SQL 支持基于实现了 SQL 标准的 Apache Calcite

- 与常规 SQL 语言中将查询指定为字符串不同，Table API 查询是以 Java 或 Scala 中的语言嵌入样式来定义的，具有 IDE 支持如:自动完成和语法检测

  ![image-20200801145413896](images/TableAPI.png)

## 10.1 需要引入的 pom 依赖

> pom.xml 老版本

```scala
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-table-planner_2.11</artifactId>
    <version>1.10.0</version>
</dependency>
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-table-api-scala-bridge_2.11</artifactId>
    <version>1.10.0</version>
</dependency>
```

> pom.xml 新版本

```xml
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-table_2.11</artifactId>
    <version>1.7.2</version>
</dependency>
```

## 10.2 简单了解 TableAPI

**基本程序结构**

- Table API 和 SQL 的程序结构，于流式处理的程序结构十分类似

```scala
val tableEnv = ... //创建表的执行环境

//创建一张表，用于读取数据
tableEnv.connect(...).createTemporaryTable("inputTable")

// 注册一张表，用于把计算结果输出
tableEnv.connect(...).createTemporaryTable("outputTable")

// 通过 Table API 查询算子，得到一张结果表
val result = tableEnv.from("inputTable").select(...)

//通过 SQL 查询语句，得到一张结果表
val sqlResult = tableEnv.sqlQuery("SELECT ... FROM inputTable ...")

//将结果表写入输出表中
result.insertInto("outputTable")
```

**创建 TableEnvironment**

- 创建表的执行环境，需要将 flink 流处理的执行环境出入

  ```scala
  val tableEnv = StreamTableEnvironment.create(env)
  ```

- ==TableEnvironment 是 flink 中集中集成 Table API 和 SQL 的核心概念==，所有对表的操作都基于 TableEnvironment
  
  - 注册 Catalog
  - 在 Catalog 中注册表
  - 执行 SQL 查询
  - 注册自定义函数（UDF）

**表（Table）**

- TableEnvironment 可以注册目录 Catalog, 并可以基于 Catalog 注册表
- 表（Table）是由一个 “标识符”（identifier）来指定，由 3 部分组成：Catalog名、数据库（database）名和对象名
- 表可以是常规的，也可以是虚拟的（视图、View）
- 常规表（Table）一般可以用来描述外部数据，比如文件、数据库表或消息队列的数据，也可以直接从 DataStream 转换而来
- 视图（View）可以从现有的表中创建，通常是 table API 或者 SQL 查询的一个结果集

**创建表**

- TableEnvironment 可以调用 .connect() 方法，连接外部系统，并调用 .createTemporaryTable() 方法，在 Catalog 中注册表

```scala
tableEnv
  .connect(...)     //定义表的数据来源，和外部系统建立连接
  .withFormat(...)  //定义数据格式化方法
  .withSchema(...)  //定义表结构
  .createTemporaryTable("MyTable") //创建临时表
```

**表的查询**

- Table API 是集成在 Scala 和 Java 语言内的查询 API
- Table API 基于代表“表” 的 Table 类，并提供一整套操作处理的方法API;这些方法会返回一个新的 Table 对象，表示对输入表应用转换操作的结果
- 有些关系型转换操作，可以由多个方法调用组成，构成链式调用结构

```scala
val senesorTable: Table = tableEnv.from("inputTable")
val resultTable: Table = sensorTable
  .select("id, temperature")
  .filter("id = sensor_1")
```

**将 DataStream 转换成表**

- 对于一个 DataStream ，可以直接转换成 Table, 进而方便地调用 Table API 做转换操作

```scala
val dataStream: DataStream[SensorReading] = ...
val sensorTable: Table = tableEnv.fromDataStream(dataStream)
```

- 默认转换后的 Table schema 和 DataStream 中的字段定义一一对应，也可以单独指定出来

  ```scala
  val dataStream: DataStream[SensorReading] = ...
  val sensorTable = tableEnv.fromDataStream(dataStream,
  					'id, 'timestamp, 'temperature)
  ```

  

**数据类型与 Schema 的对应**

- DataStream  中的数据类型，与表的 Schema 之间的对应关系，可以有两种基于字段名称，或者基于字段位置

- 基于名称（name-based）

  ```scala
  val sensorTable = tableEnv.fromDataStream(dataStream,
  					'timestamp as 'ts, 'id as 'myId,'temperature')
  ```

- 基于位置

  ```scala
  val sensorTable = tableEnv.fromDataStream(dataStream, 'myId, 'ts)
  ```

**创建临时视图**

- 基于 DataStream 创建临时视图

  ```scala
  tableEnv.createTemporaryView("sensorView", dataStream)
  
  tableEnv.createTemporaryView("sensorView",
  	dataStream, 'id, 'temperature, 'timestamp as 'ts)
  ```

- 基于 table 创建临时视图

  ```scala
  tableEnv.createTemporaryView("sensorView", sensorTable)
  ```

**输出表**

- 表的输出是通过将数据写入 TableSink 来实现的

- TableSink 是一个通用接口，可以支持不同的文件格式、存储数据库和消息队列

- 输出表最直接的方法，就是通过 Table.insertinto() 方法将一个 Table 写入注册过的 TableSink  中

  ```scala
  tableEnv.connect(...)
  	.createTemporaryTable("outputTable")
  val resultSqlTable: Table = ...
  resultTable.insertInto("outputTable")
  ```

**更新模式**

- 对于流式查询，需要声明如何在表和外部连接器之间执行转换
- 与外部系统交换的消息类型，由更新冒失（Update Mode） 指定
  - 追加（Append）模式
    - 表只做插入操作，和外部连接器酯交换插入（insert）消息
  - 撤回（Retract）模式
    - 表和外部连接器交换添加（Add）和撤回（Retract）消息
    - 插入操作（insert）编码为 Add 消息；删除（Delete）编码为 Retract 消息，更新（Update）编码为上一条的 Retract 和下一条的 Add 消息
  - 更新插入（Upset）模式
    - 更新和插入都被编码为 Upset 消息，删除编码为 Delete 消息

**输出到 Kafka**

- 可以创建 Table 来描述 kafka 中的数据，作为输入或输出的 TableSink

  ```scala
  tableEnv.connect(
    new Kafka()
     .version("0.11")
     .topic("sinkTest")
     .property("zookeeper.connect", "localhost:2181")
     .property("bootstrap.servers", "localhost:9092")
  )
    .withFormat(new Csv())
    .withScahema(new Schema()
     .field("id", DataTypes.STRING())
     .field("temp", DataTypes.DOUBLE())
     )
    .createTemporaryTable("KafkaOutputTable")
  resultTable.insertInto("KafkaOutputTable")
  ```

**输出到 ES**

- 可以创建 Table 来描述 ES 中的数据，作为输入或输出的 TableSink

  ```scala
  tableEnv.connect(
    new Elasticsearch()
     .version("6")
      .host("localhost", 9200, "http")
      .index("sensor")
      .documentType("temp")
  )
    .inUpsertNode()
    .withFormat(new Json())
    .withScahema(new Schema()
     .field("id", DataTypes.STRING())
     .field("temp", DataTypes.DOUBLE())
     )
    .createTemporaryTable("esOutputTable")
  aggResultTable.insertInto("esOutputTable")
  ```

**输出到 MySQL**

- 可以创建 Table 来描述 MySQL中的数据，作为输入和输出

  ```scala
  val sinkDDL: String = """
    |create table jdbcOutputTable (
    |  id varchar(20) not null,
    |  cnt bigint not null
    | ) with (
    |  'connector.type' = 'jdbc',
    |  'connector.url' = 'jdbc:mysql://localhost:3306/test',
    |  'connector.table' = 'sensor_count',
    |  'connector.driver' = 'com.mysql.jdbc.Driver',
    |  'connector.username' = 'jdbc',
    |  'connector.password' = 'root'
    | )
    """.stripMargin
    
   tableEnv.sqlUpdate(sinkDDL) //执行 DDL 创建表
   aggResultSqlTable.insertInto("jdbcOutputTable")
  ```

**时间特性（Time Attributes）**

- 基于时间的操作（比如 Table API he SQL 中窗口操作），需要定义相关的时间语义和时间数据来源的信息
- Table 可以提供一个逻辑上的时间字段，用于在表处理程序中，只是时间和访问相应的时间戳
- 时间属性，可以是每个表 schema 的一部分。一旦定义了时间属性，他就可以作为一个字段引用，并且可以在基于时间的操作中使用
- 事件属性的行为类似于常规时间戳，看可以访问，并且进行计算

**定义处理时间**

- 处理时间语义下，允许表处理程序根据机器的本地时间生成结果。他是时间的最简单概念。它既不需要提取时间戳，也不需要生成 watermark

由于DataStream 转换成表时间指定

- 在定义 Schema 期间，可以使用 .proctime, 指定字段名定义处理时间字段

- 这个 proctime 属性只能通过附加逻辑字段，来扩展物理 schema。因此，只能在 schema 定义的末尾定义它

  ```scala
  val sensorTable = tableEnv.fromDataStream(dataStream,
   'id,'temperature, 'pt.proctime)
  ```

**定义处理时间（Processing Time）**

- 定义 Table Schema 时指定

  ```scala
   .withScahema(new Schema()
    .field("id", DataTypes.STRING())
    .field("temstamp", DataTypes.BIGINT())
    .field("temperature", DataTypes.TIMESTAMP(3))
    .field("pt", DataTypes.DOUBLE())
    .proctime()
  )
  ```

- 在创建表的 DDL 中定义

```scala
val sinkDDL: String = """
  |create table jdbcOutputTable (
  |  id varchar(20) not null,
  |  ts bigint
  |  temperature double,
  |  pt AS PROCTIME()
  | ) with (
  |  'connector.type' = 'filesystem',
  |  'connector.path' = '/sensor.txt',
  |  'format.type' = 'csv',
  | )
  """.stripMargin
  
 tableEnv.sqlUpdate(sinkDDL)
```

**定义事件时间**

- 定义 Table Schema 时指定

```scala
 .withScahema(new Schema()
  .field("id", DataTypes.STRING())
  .field("temstamp", DataTypes.BIGINT())
  .rowtime(
  	new Rowtime()
      .timestampFromField("timestamp")  //从字段中提取时间戳
      .watermarksPeriodicBounded(1000)  //watermark 延迟 1 秒
  )
  .field("temperature", DataTypes.TIMESTAMP(3))
)
```

**窗口**

- 时间语义，要配合窗口操作才能发挥作用
- 在 Table API 和 SQL 中，主要有两种窗口

Group Windows （分组窗口）

- 根据时间或行计数间隔，将行距和到有限的组（Group）中，并对每个组的数据执行一次聚合函数

Over Windows

- 针对每个输入行，计算相邻行范围内的聚合

- 收到

**Group Window**

- Group Window 是使用 window（w:GroupWindow）自居定义的，并且必须由 as 自居指定一个别名
- 为了按窗口对表进行分组，窗口的别名必须在 group by 子句中，像常规的分组字段一样引用

```
val table = input
  .window([w: GroupWindow] as 'w) //定义窗口，别名为 w
  .groupBy('w, 'a)  //按照字段 a 和窗口 w 分组
  .select('a, 'b.sum) //聚合
```

- Table API 提供了一组具有特定语义的预定义 Window 类，这些类会被转换为底层 DataStream 或 DataSet 的窗口操作

```scala
def main(args: Array[String]): Unit = {
    
    val env: StreamExecutionEnvironment =
    StreamExecutionEnvironment.getExecutionEnvironment
    
    val myKafkaConsumer: FlinkKafkaConsumer011[String] =
    MyKafkaUtil.getConsumer("ECOMMERCE")
    
    val dstream: DataStream[String] = env.addSource(myKafkaConsumer)
    
    val tableEnv: StreamTableEnvironment =
    TableEnvironment.getTableEnvironment(env)
    
    val ecommerceLogDstream: DataStream[EcommerceLog] = dstream.map{
    jsonString => JSON.parseObject(jsonString,classOf[EcommerceLog]) }
    
    val ecommerceLogTable: Table =
    tableEnv.fromDataStream(ecommerceLogDstream)
    
    val table: Table = ecommerceLogTable.select("mid,ch").filter("ch ='appstore'")
    val midchDataStream: DataStream[(String, String)] =
    table.toAppendStream[(String,String)]
    
    midchDataStream.print()
    
    env.execute()
}
```

**滚动窗口（Tumbling windows）**

- 滚动窗口要用 Tumble 类来定义

```scala
//Tumbling Event-time Window
 .window(Tumble over 10.minnutes on 'rowtime as 'w)
 
//Tumbling Processing-time Window
 .window(Tumble over 10.minutes on 'proctime as 'w)

//Tumbling Row-count window
 .window(Tumble over 10.rows on 'proctime as 'w)
```

**滑动窗口（Sliding windows）**

- 华东窗口要用 Slice 类来定义

```scala
// Sliding Event-time Window
 .window(Slice over 10.minutes every 5.minutes on 'rowtime as 'w)
 
// Sliding Processing-time Window
 .window(Slice over 10.minutes every 5.minutes on 'proctime as 'w)

// Sliding Row-count Window
 .window(Slice over 10.rows every 5.rows on 'proctime as 'w)
```

**会话窗口（Session windows）**

- 会话窗口要用 Seesion 类来定义

```scala
//Seesion Event-time Window
 .window(Seesion withGap 10.minutes on 'rowtime as 'w)
 
//Seesion Processing-time Window
 .window(Seesion withGap 10.minutes on 'proctime as 'w) 
```

**Over Window**

- Over window 聚合是标准 SQL 中已有的（over子句），可以在查询的 SELECT 子句中定义
- Over window 聚合，会针对每个输入行，计算相邻行范围内的聚合
- Over Windows 使用 window (w:overwindows*) 子句定义，并在 select() 方法中通过别名来引用

```scala
val table = input 
  .window([w: OverWindow] as 'w)
  .select('a, 'b.sum over 'w. 'c.min over 'w)
```

- Table API 提供了 Over 类，来配置 Over 窗口的属性

**无界 Over Windows**

- 可以在事件时间或处理时间，以及指定为时间间隔、或行计数的范围内，定义 Over windows
- 无界的 Over window 是使用常量指定的

```scala
//无界的事件时间 over window
 .window(Over partitionBy 'a orderBy 'rowtime preceding UNBOUNDED_RANGE as 'w)
 
//无界的处理时间 over window
 .window(Over partitionBy 'a orderBy 'proctime preceding UNBOUNDED_RANGE as 'w) 

//无界的事件时间 Row-count over window
 .window(Over partitionBy 'a orderBy 'rowtime preceding UNBOUNDED_ROW as 'w) 
 
//无界的处理时间 Row-count over window
 .window(Over partitionBy 'a orderBy 'proctime preceding UNBOUNDED_ROW as 'w) 
```

**有界 Over Windows**

- 有界的 over window 是用间隔的大小指定的

```scala
//有界的事件时间 over window
 .window(Over partitionBy 'a orderBy 'rowtime preceding 1.minutes as 'w)
 
//有界的处理时间 over window
 .window(Over partitionBy 'a orderBy 'proctime preceding 1.minutes as 'w) 

//有界的事件时间 Row-count over window
 .window(Over partitionBy 'a orderBy 'rowtime preceding 10.rows as 'w) 
 
//有界的处理时间 Row-count over window
 .window(Over partitionBy 'a orderBy 'proctime preceding 10.rows as 'w) 
```

**SQL 中的 Group Windowa**

- Group Windows 定义在 SQL 查询的 Group By 子句中

TUMBLE(time_attr, interval)

- 定义了一个滚动窗口，第一个参数是时间字段，第二个参数是窗口长度

HOP(time_attr, interval, interval)

- 定义了一个滑动窗口，第一个参数是时间字段，第二个参数是窗口滑动步长，第三个是窗口长度

SESSION(time_attr, interval)

- 定义了一个会话窗口，第一个参数是时间字段，第二个参数是窗口间隔

**SQL 中的 Over Windows**

- 用 Over 做窗口聚合时，所有聚合必须在同一个窗口上定义，也就是说必须是相同的分区、排序和范围
- 目前仅支持在当前行范围之前的窗口
- ORDER BY 必须在单一的时间属性上指定

```sql
SELECT COUNT(amount) OVER (
	PARTITION BY user
	ORDER BY proctime
	ROWS BETWEEN 2 PRECEDING AND CURRENT ROW)
FROM Oders
```

**函数（Functions）**

- Flink Table API 和 SQL 为用户提供了一组用以数据转换的内置函数
- SQL 中支持的很多函数，Table API 和 SQL 都已经做了实现

比较函数

- SQL
  - value1 = value2
  - value1 > value2
- Table API
  - ANY1 === ANY2
  - ANY1 > ANY2

逻辑函数

- SQL
  - boolean1 OR boolean2
  - boolean IS FALSE
  - NOT boolean
- Table API
  - BOOLEAN1||BOOLEAN2
  - BOOLEAN.isFalse
  - !BOOLEAN

算数函数

- SQL
  - numeirc1 + numeric2
  - POWER(numeric1, numeric2)
- Table API
  - NUMERIC1 + NUMERIC2
  - NUMERIC1.power(NUMERIC2)

字符串函数

- SQL
  - string1 || string2
  - UPPER(string)
  - CHAR_LENGTH(thing)
- Table API
  - STRING1 + STRING2
  - STRING.upperCase()
  - STRING.charLength()

时间函数

- SQL
  - DATE string
  - TIMESTAMP string
  - CURRENT_TIME
  - INTERVAL string range
- Table API
  - STRING.toDate
  - STRING.Timestamp
  - currentTime()
  - NUMEIRC.days
  - NUMERIC.minutes

聚合函数

- SQL
  - COUNT(*)
  - SUM(expression)
  - RANK()
  - ROW_NUMEIRC()
- Table API:
  - FIELD.count
  - FIELD.sum()

用户自定义函数（UDF）

- 用户定义函数（User-defined Functions, UDF）是一个重要的特性，它们显著地扩展了查询的表达能力
- 在大多数情况下，用户定义的函数必须先注册，然后才能在查询中使用
- 函数通过调用 registerFunction() 方法在TableEnvitonment 中注册。当用户定义的函数被注册时，它被插入到了 TableEnvionment 的函数目录中，这样 Table API 或 SQL 解析器就可以识别并正确地解释它。

标量函数（Scalar Functions）

- 用户定义的标量函数，可以将 0、1或多个标量值
- 为了定义标量函数，必须在 org.apache.flink.table.functions 中扩展基类 Scalar Function, 并实现（一个或多个）求值（eval）方法

```scala
class HashCode( factor: Int) extends ScalarFunction {
	def eval( s: String): Int = {
		s.hashCode * factor
	}
}
```

**表函数（Table Functions）**

- 用户定义的表函数，也可以将 0、1或多个标量值作为输入参数；与标量函数不同的是，他可以返回任意数量的行作为输出，而不是单个值
- 为了定义一个表函数，必须扩展 org.apche.flink.table.functions 中的积累 TableFunction 并实现（一个或多个）求值方法
- 表函数的行为由其求值方法决定，求值方法必须是 public 的，并命名为 eval

```scala
class Split(separator: String) extends TableFunction[(String, Int)]{
 def eval(str: String): Unit = {
   str.split(separator).foreach(
    word => collect((word, word.length))
   )
 }}
```

**聚合函数（Aggregate Functions）**

- 用户自定义聚合函数（User-DefinedAggregate Functions, UDAGGs）可以把一个表中的数据，聚合成一个标量值
- 用户自定义聚合函数，是通过继承 AggregateFuntion 抽象类实现的

![image-20200802014302426](images/Aggregate-Functions.png)

- AggregationFuntion 要求必须实现的返回给发：
  - createAccumulator()
  - accumulate()
  - getValue()
- AggregateFunction 的工作原理
  - 首先，他需要一个累加器（Accumulator）, 用来保存聚合中间结果的数据结构；可以通过调用 createAccumulator() 方法创建空累加器
  - 随后，对每个输入行调用函数的 accumulate() 方法来更新累加器
  - 处理完所有行后，将调用函数的 getValue() 方法来计算并返回最终结果

**表聚合函数（Table Aggregate Functions）**

- 用户定义的表聚合函数（User-Defined Table Aggregate FUntions, UDTAGGs）,可以把一个表中数据，聚合为具有多行和多列的结果表
- 用户定义表聚合函数，是通过继承 TableAggregateFunction 抽象类来实现的

![image-20200802020640762](images/Table-Aggregate-Functions.png)

### 10.2.1 动态表

> 如果流中的数据类型是 case class 可以直接根据 case class 的结构生成 table

```scala
tableEnv.fromDataStream(ecommerceLogDstream)
```

> 根据字段顺序单独命名

```scala
tableEnv.fromDataStream(ecommerceLogDstream,’mid,’uid .......)
```

> 最后的动态表可以转换为流进行输出

```
table.toAppendStream[(String,String)]
```

### 10.2.2  字段

> 一个单引放到字段前面来标识字段名, 如 ‘name , ‘mid ,’amount 等

## 10.3 TableAPI 的窗口聚合操作

### 10.3.1 通过一个例子了解 TableAPI

```scala
// 每 10 秒中渠道为 appstore 的个数
def main(args: Array[String]): Unit = {
    //sparkcontext
    val env: StreamExecutionEnvironment =
    StreamExecutionEnvironment.getExecutionEnvironment
    
    // 时间特性改为 eventTime
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    val myKafkaConsumer: FlinkKafkaConsumer011[String] =
    MyKafkaUtil.getConsumer("ECOMMERCE")
    
    val dstream: DataStream[String] = env.addSource(myKafkaConsumer)
    val ecommerceLogDstream: DataStream[EcommerceLog] = dstream.map{ jsonString
    =>JSON.parseObject(jsonString,classOf[EcommerceLog]) }
    
    // 告知 watermark  和 eventTime 如何提取
    val ecommerceLogWithEventTimeDStream: DataStream[EcommerceLog] =
    ecommerceLogDstream.assignTimestampsAndWatermarks(new
    BoundedOutOfOrdernessTimestampExtractor[EcommerceLog](Time.seconds(0L)) {
        override def extractTimestamp(element: EcommerceLog): Long = {
        	element.ts
        }
    }).setParallelism(1)
    
    val tableEnv: StreamTableEnvironment = TableEnvironment.getTableEnvironment(env)
    
    // 把数据流转化成 Table
    val ecommerceTable: Table =
    tableEnv.fromDataStream(ecommerceLogWithEventTimeDStream ,
    'mid,'uid,'appid,'area,'os,'ch,'logType,'vs,'logDate,'logHour,'logHourMinut
    e,'ts.rowtime)
    
    // 通过 table api  进行操作
    //  每 10 秒 统计一次各个渠道的个数 table api  解决
    //1、groupby 2、要用 window 3、用 eventtime 来确定开窗时间
    val resultTable: Table = ecommerceTable.window(Tumble over 10000.millis on
    'ts as 'tt).groupBy('ch,'tt ).select( 'ch, 'ch.count)
    
    // 把 Table 转化成数据流
    val resultDstream: DataStream[(Boolean, (String, Long))] =
    resultSQLTable.toRetractStream[(String,Long)]
    resultDstream.filter(_._1).print()
    env.execute()
}
```

### 10.3.2  关于 group by

> 1. 如果了使用 groupby，table 转换为流的时候只能用 toRetractDstream

```scala
val rDstream: DataStream[(Boolean, (String, Long))] = table
	.toRetractStream[(String,Long)]
```

> 2. toRetractDstream 得到的第一个 boolean 型字段标识 true 就是最新的数据(Insert)，false 表示过期老数据(Delete)

```scala
val rDstream: DataStream[(Boolean, (String, Long))] = table
	.toRetractStream[(String,Long)]
rDstream.filter(_._1).print()
```

> 3. 如果使用的 api 包括时间窗口，那么窗口的字段必须出现在 groupBy 中。

```scala
val table: Table = ecommerceLogTable
    .filter("ch ='appstore'")
    .window(Tumble over 10000.millis on 'ts as 'tt)
    .groupBy('ch ,'tt)
    .select("ch,ch.count ")
```

### 10.3.3  关于时间窗口

> 1. 用到时间窗口，必须提前声明时间字段，如果是 processTime 直接在创建动态表时进行追加就可以

```scala
val ecommerceLogTable: Table = tableEnv
  .fromDataStream( ecommerceLogWithEtDstream,
	'mid,'uid,'appid,'area,'os,'ch,'logType,'vs,'logDate,'logHour,'lo
gHourMinute,'ps.proctime)
```

> 2. 如果是 EventTime 要在创建动态表时声明

```scala
val ecommerceLogTable: Table = tableEnv
 .fromDataStream(ecommerceLogWithEtDstream,
	'mid,'uid,'appid,'area,'os,'ch,'logType,'vs,'logDate,'logHour,'lo
gHourMinute,'ts.rowtime)
```

> 3. 滚动窗口可以使用 Tumble over 10000.millis on 来表示

```scala
val table: Table = ecommerceLogTable.filter("ch ='appstore'")
 .window(Tumble over 10000.millis on 'ts as 'tt)
 .groupBy('ch ,'tt)
 .select("ch,ch.count ")
```

## 10.4 SQL 如何编写

```scala
def main(args: Array[String]): Unit = {
    //sparkcontext
    val env: StreamExecutionEnvironment =
    StreamExecutionEnvironment.getExecutionEnvironment
    
    // 时间特性改为 eventTime
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    
    val myKafkaConsumer: FlinkKafkaConsumer011[String] =
    MyKafkaUtil.getConsumer("ECOMMERCE")
    
    val dstream: DataStream[String] = env.addSource(myKafkaConsumer)
    
    val ecommerceLogDstream: DataStream[EcommerceLog] = dstream.map{ jsonString
    =>JSON.parseObject(jsonString,classOf[EcommerceLog]) }
    
    // 告知 watermark  和 eventTime 如何提取
    val ecommerceLogWithEventTimeDStream: DataStream[EcommerceLog] =
    ecommerceLogDstream.assignTimestampsAndWatermarks(new
    BoundedOutOfOrdernessTimestampExtractor[EcommerceLog](Time.seconds(0L)) {
        override def extractTimestamp(element: EcommerceLog): Long = {
            element.ts
        }
    }).setParallelism(1)

    //SparkSession
    val tableEnv: StreamTableEnvironment = TableEnvironment.getTableEnvironment(env)
    
    // 把数据流转化成 Table
    val ecommerceTable: Table =
    tableEnv.fromDataStream(ecommerceLogWithEventTimeDStream ,
    'mid,'uid,'appid,'area,'os,'ch,'logType,'vs,'logDate,'logHour,'logHourMinu
    te,'ts.rowtime)
    
    // 通过 table api  进行操作
    //  每 10 秒 统计一次各个渠道的个数 table api  解决
    //1 groupby 2  要用 window 3  用 eventtime 来确定开窗时间
    val resultTable: Table = ecommerceTable.window(Tumble over 10000.millis on
    'ts as 'tt).groupBy('ch,'tt ).select( 'ch, 'ch.count)
    
    //  通过 sql  进行操作
    val resultSQLTable : Table = tableEnv.sqlQuery( "select ch ,count(ch) from
    "+ecommerceTable+" group by ch ,Tumble(ts,interval '10' SECOND )")
    
    // 把 Table 转化成数据流
    //val appstoreDStream: DataStream[(String, String, Long)] =
    appstoreTable.toAppendStream[(String,String,Long)]
    
    val resultDstream: DataStream[(Boolean, (String, Long))] =
    resultSQLTable.toRetractStream[(String,Long)]
    
    resultDstream.filter(_._1).print()
    
    env.execute()
}
```



# 第 11 章 Flink CEP 简介

## 11.1 什么是复杂事件处理 CEP

> 特征

- 复杂事件处理（Complex Event Processing, CEP）。
- Flink CEP 是在 Flink 中实现的复杂时间处理（CEP）库。
- CEP 允许在无休止的事件流中检测事件模式，让我们有机会掌握数据中重要的部分。
- 一个或多个由简单事件构成的事件流通过一定的规则匹配，然后输出用户想要得到的数据——满足规则的复杂事件。

![image-20200801123024497](images/CEP.png)

- 目标：从有序的简单事件流中发现一些高阶特征
- 输入：一个或多个由简单事件构成的事件流
- 处理：识别简单事件之间的内在联系，多个符合一定规则的简单事件构成复杂事件
- 输出：满足规则的复杂事件

-----

​		==CEP 用于分析低延迟、频繁产生的不同来源的事件流==。CEP 可以帮助在复杂的、不相关的事件流中找出有意义的模式和复杂的关系，以接近实时或准实时的获得通知并阻止一些行为。

​		CEP 支持在流上进行模式匹配，根据模式的条件不同，分为连续的条件或不连续的条件；模式的条件允许有时间的限制，当在条件范围内没有达到满足的条件时，会导致模式匹配超时。

看起来很简单，但是它有很多不同的功能：

-  输入的流数据，尽快产生结果；
- 在 2 个 event 流上，基于时间进行聚合类的计算；
- 提供实时/准实时的警告和通知；
- 在多样的数据源中产生关联并分析模式；
- 高吞吐、低延迟的处理；

----

​		市场上有多种 CEP 的解决方案，例如 Spark、Samza、Beam 等，但他们都没有提供专门的 library 支持。但是 Flink 提供了专门的 CEP library。

----

**Pattern API**

- 处理事件的规则，被叫做 ”模式“ （Pattern）
- Flink CEP 提供了 Pattern API，用于对输入流数据进行复杂事件规则定义，用来提取符合规则的事件序列

```scala
//定义一个 Pattern
val pattern = Pattern.begin[Event]("start").where(_.getId == )
 .next("mindle").subtype(classof[SubEvent]).where(_.getTemp >= 10.0)
 .followedBy("end").where(_.getName == "end")

//将创建好的 Pattern 应用到输入事件流上
val patternStream = CEP.pattern(inputDataStream, pattern)

//获取事件序列，得到处理结果
val result: DataStream[Alert] = patternStream.select(createdAlert(_))
```

----

- 个体模式（Individual Patterns）

  - 组成复杂规则的每一个单独的模式定义，就是”个体模式“

  ```scala
  start.times(3).where(_.behavior.startsWith("fav"))
  ```

  - 个体模式可以包括 ”单例（singleon）模式“ 和 ”循环（looping）模式“

  - 单例模式只接收一个事件，而循环模式可以接收多个

  - 量词（Quantifier）

    - 可以在一个个体模式后追加量词，也就是指定循环次数

    ```scala
    //匹配出现 4 次
    start.times(4)
    //匹配出现 0 或 4 次
    start.times(4).optional
    //匹配出现 2,3 或 4 次
    start.times(2,4)
    //匹配出现 0 或 4 次，并且尽可能多地重复匹配
    start.times(2,4).greedy
    //匹配出现 1 次或多次
    start.timesOrMore
    //匹配出现 0 次、2 次 或多次，并且尽可能多地重复匹配
    start.timesOrMore(2).optional.greedy
    ```

  - 个体模式的条件

    - 条件（Condition）

      - 每个条件都需要指定触发条件，作为模式是否接受事件进入的判断依据

      - CEP 中的个体模式主要通过调用 .where() ,or() 和 .until() 来指定条件

      - 按不同的调用方式，可以分成以下几类：

        - 简单条件（SImple Condition）

          - 通过 .where() 方法对事件中的字段进行判断筛选，决定是否接受该事件

          ```scala 
          pattern.where(event => ... /* some condition */).or(event => .. /* or condition */)
          ```

          - 组合条件（Combining Condition）

            - 将简单条件进行合并；.or() 方法表示或逻辑相连， where 的直接组合就是 AND

              ```scala
              start.where( event => event.getName.startWith("foo"))
              ```

          - 终止条件（Stop Condition）

            - 如果使用了 oneOrMore 或者 oneOrMore.optional, 建议使用 .until 作为终止条件，以便清理状态

          - 迭代条件（Iterative Condition）

            - 能够对模式之前所有接收的事件进行处理
            - 调用 .where( (value, ctx) => {...}), 可以调用 ctx.getEventsForPattern("name")

----

- 组合模式（Combining Patterns, 也叫模式序列）

  - 不同的 "近邻" 模式

  ![image-20200801131349963](images/Combining-Patterns.png)

     - 严格近邻（Strict Contiguity）

       			- 所有事件按照严格的顺序出现，中间没有任务不匹配的事件，由 .next() 指定
            			- 例如对于模式 ”a next b“, 事件序列 [a, c, b1, b2]没有匹配

  - 宽松近邻（Relaxed Contiguity）

    - 允许中间出现不匹配的事件，由 .followedBy() 指定
    - 例如对于模式 ” a followedBy b ",  事件序列 [a, c, b1, b2]匹配为 {a, b1}

  - 非确定性宽松紧邻（Non-Deterministic Relaxed Contiguity）

    - 进一步放宽条件，之前已经匹配过的事件也可以再次使用，由 .followdByAny() 指定
    - 例如对于模式  ” a followedByAny b ", 事件序列 [a, c, b1, b2]匹配为 {a, b1},  {a, b2}

  - 除以上模式序列外，还可以定义 “不希望出现某种近邻关系”

    -  .notNexr()——不想让某个事件严格按照紧邻前一个事件发生
    - .notFollowdBy()——不想让某个事件在两个事件之间发生

  - 需要注意：

    - 所有模式序列必须以 .begin() 开始
    - 模式序列不能以 .notFollowedBy() 结束
    - “not” 类型的模式不能被 optional 所修饰
    - 此外，还可以为模式指定时间约束，用来要求在多长时间内匹配有效

  - 模式的检测

    - 指定要查找的模式序列后，就可以将其应用于输入流以检测潜在匹配
    - 调用 CEP.pattern()，给定输入流和模式，就能得到一个 PatternStream

    ```scala
    val inout : DataStream[Event] = ...
    val pattern : Pattern[Event,_] = ...
    
    val patternStream: PatternStream[Event] = CEP.pattern(input, pattern)
    ```

  - 匹配事件的提取

    - 创建 PatternStream 之后，就可以应用 select 或者 flatselect 方法，从检测到的事件序列中提取事件了
    - select() 方法需要输入一个 select function 作为参数，每个成功匹配的事件序列都会调用它
    - select() 以一个 Map[String, Iterable[IN]] 来接收匹配到的事件序列，其中 key 就是每个模式的名称，而 value 就是所有接收到的事件的 Iterable 类型

    ```scala
    def selectFn(pattern : Map[tring, Iterable[IN]]): OUT = {
    	val startEvent = pttern.get("start").get.next
    	val endEvent = pattern.get("end").get.next
    	OUT(startEvent, endEvent)
    } 
    ```

  - 超时事件的提取

    - 当一个模式通过 within 关键字定义了检测窗口时间时，部分事件序列可能因为超过了窗口长度而被放弃；为了能够处理这些超时的部分匹配， select 和 flatSelect API 调用允许指定超时处理程序
    - 超时处理程序会接收到目前为止由模式匹配到的所有事件，由一个 OutputTag 定义接收到的超时事件序列

    ```scala
    val patternStream: PatternStream[Event] = CEP.pattern(input, pattern)
    
    val outputTag = OutputTag[String]("side-output")
    
    val result = patternStream.select(outputTag){
    	(pattern: Map[String, Iterable[Event]], timestamp: Long) => TimeoutEvent()
    }{
    		pattern: Map[String, Iterable[Event]] => ComplexEvent()	
      }
    
    val timeoutResult: DataStream<TimeoutEvent> = result.getSideOutput(outputTag)
    ```

    

  ----

  

  - 很多个体模式组合起来，就形成了整个的模式序列
  - 模式序列必须以一个 ”初始模式“ 开始

  ```scala
  val start = Pattern.begin("start")
  ```

- 模式组（Groups of patterns）

  - 将一个模式序列作为条件嵌套在个体模式里，称为一组模式

## 11.2 Flink CEP



# 附录 常见面试问题汇总

## 1．面试题一：应用架构

问题：公司怎么提交的实时任务，有多少 Job Manager？
解答： 

1. 我们使用 yarn session 模式提交任务。每次提交都会创建一个新的 Flink 集群，为每一个 job 提供一个 yarn-session，任务之间互相独立，互不影响，方便管理。任务执行完成之后创建的集群也会消失。线上命令脚本如下：

```sh
bin/yarn-session.sh -n 7 -s 8 -jm 3072 -tm 32768 -qu root.*.* -nm *-* -d
```


其中申请 7 个 taskManager，每个 8 核，每个 taskmanager 有 32768M 内存。

2. 集群默认只有一个 Job Manager。但为了防止单点故障，我们配置了高可用。我们公司一般配置一个主 Job Manager，两个备用 

Job Manager，然后结合 ZooKeeper 的使用，来达到高可用。

## 2．面试题二：压测和监控

问题：怎么做压力测试和监控？

解答：我们一般碰到的压力来自以下几个方面：
一，产生数据流的速度如果过快，而下游的算子消费不过来的话，会产生背压。
背压的监控可以使用 Flink Web UI(localhost:8081) 来可视化监控，一旦报警就能知道。一般情况下背压问题的产生可能是由于 sink 这个操作符没有优化好，
做一下优化就可以了。 比如如果是写入 ElasticSearch， 那么可以改成批量写入，可以调大 ElasticSearch 队列的大小等等策略。

二，设置 watermark 的最大延迟时间这个参数，如果设置的过大，可能会造成内存的压力。可以设置最大延迟时间小一些，然后把迟到元素发送到侧输出流中去。
晚一点更新结果。或者使用类似于 RocksDB 这样的状态后端， RocksDB 会开辟堆外存储空间，但 IO 速度会变慢，需要权衡。

三，还有就是滑动窗口的长度如果过长，而滑动距离很短的话，Flink 的性能
会下降的很厉害。我们主要通过时间分片的方法，将每个元素只存入一个“重叠窗口”，这样就可以减少窗口处理中状态的写入。
参见链接：https://www.infoq.cn/article/sIhs_qY6HCpMQNblTI9M

四，状态后端使用 RocksDB，还没有碰到被撑爆的问题。

## 3．面试题三：为什么用 Flink

问题：为什么使用 Flink 替代 Spark？

解答：

主要考虑的是 flink 的低延迟、高吞吐量和对流式数据应用场景更好的支持；另外，flink 可以很好地处理乱序数据，而且可以保证 exactly-once 的状态一致性。

## 4．面试题四：checkpoint 的存储

问题：Flink 的 checkpoint 存在哪里？
解答：可以是内存，文件系统，或者 RocksDB。

## 5．面试题五：exactly-once 的保证

问题：如果下级存储不支持事务，Flink 怎么保证 exactly-once？
解答：端到端的 exactly-once 对 sink 要求比较高，具体实现主要有幂等写入和事务性写入两种方式。
==幂等写入的场景依赖于业务逻辑==，更常见的是用事务性写入。而==事务性写入又有预写日志（WAL）和两阶段提交（2PC）两种方式==。
如果外部系统不支持事务，那么可以用预写日志的方式，把结果数据先当成状态保存，然后在收到 checkpoint 完成的通知时，一次性写入 sink 系统。

## 6．面试题六：状态机制

问题：说一下 Flink 状态机制？

解答：Flink 内置的很多算子，包括源 source，数据存储 sink 都是有状态的。在 Flink 中，状态始终与特定算子相关联。Flink 会以checkpoint 的形式对各个任务的状态进行快照，用于保证故障恢复时的状态一致性。Flink 通过状态后端来管理状态和 checkpoint 的存储，状态后端可以有不同的配置选择。

## 7．面试题七：海量 key 去重

问题：怎么去重？考虑一个实时场景：双十一场景，滑动窗口长度为 1 小时，滑动距离为 10 秒钟，亿级用户，怎样计算 UV？
解答：使用类似于 scala 的 set 数据结构或者 redis 的 set 显然是不行的，因为可能有上亿个 Key，内存放不下。所以可以考虑使用布隆过滤器（Bloom Filter）来去重。

## 8．面试题八：checkpoint 与 spark 比较

问题：Flink 的 checkpoint 机制对比 spark 有什么不同和优势？

解答：spark streaming 的 checkpoint 仅仅是针对 driver 的故障恢复做了数据和元数据的 checkpoint。而 flink 的 checkpoint 机制 要复杂了很多，它采用的是轻量级的分布式快照，实现了每个算子的快照，及流动中的数据的快照。

参见文档 https://cloud.tencent.com/developer/article/1189624

## 9．面试题九：watermark 机制

问题：请详细解释一下 Flink 的 Watermark 机制。
解答：Watermark 本质是 Flink 中衡量 EventTime 进展的一个机制，主要用来处理乱序数据。

## 10．面试题十：exactly-once 如何实现

问题：Flink 中 exactly-once 语义是如何实现的，状态是如何存储的？

解答：Flink 依靠 checkpoint 机制来实现 exactly-once 语义，如果要实现端到端的 exactly-once，还需要外部 source 和 sink 满足一定

的条件。状态的存储通过状态后端来管理，Flink 中可以配置不同的状态后端。详见文档 9.2、9.3 及 9.4 节。

## 11．面试题十一：CEP

问题：Flink CEP 编程中当状态没有到达的时候会将数据保存在哪里？
解答：在流式处理中，CEP 当然是要支持 EventTime 的，那么相对应的也要支持数据的迟到现象，也就是 watermark 的处理逻辑。CEP 

对未匹配成功的事件序列的处理，和迟到数据是类似的。在 Flink CEP 的处理逻辑中，状态没有满足的和迟到的数据，都会存储在一个 

Map 数据结构中，也就是说，如果我们限定判断事件序列的时长为 5 分钟，那么内存中就会存储 5 分钟的数据，这在我看来，也是对内

存的极大损伤之一。

## 12．面试题十二：三种时间语义

问题：Flink 三种时间语义是什么，分别说出应用场景？

解答：

1. Event Time：这是实际应用最常见的时间语义，事件创建的时间。
2. Processing Time：执行操作算子的本地系统时间，与机器相关, 没有事件时间的情况下，或者对实时性要求超高的情况下。
3. Ingestion Time：数据进入 Flink 的时间, 存在多个 Source Operator 的情况下，每个 Source Operator可以使用自己本地系统时钟指 派 Ingestion Time。后续基于时间相关的各种操作，都会使用数据记录中的 Ingestion Time。

## 13．面试题十三：数据高峰的处理

问题：Flink 程序在面对数据高峰期时如何处理？

解答：使用大容量的 Kafka 把数据先放到消息队列里面作为数据源，再使用 Flink 进行消费，不过这样会影响到一点实时性。







































































































































































































